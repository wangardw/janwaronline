package com.wangard.bakra.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.ProgressBar;

import com.google.gson.JsonObject;
import com.wangard.bakra.Activities.Auth.Login;
import com.wangard.bakra.Adapters.AdapterAnimals;
import com.wangard.bakra.AppController;
import com.wangard.bakra.Models.Animals.AnimalItem;
import com.wangard.bakra.Models.Animals.AnimalsResponse;
import com.wangard.bakra.Models.Seller.SellerData;
import com.wangard.bakra.Models.Seller.SellerResponse;
import com.wangard.bakra.R;
import com.wangard.bakra.Utils.Constants;
import com.wangard.bakra.Utils.SnappyDBUtil;
import com.wangard.bakra.Utils.Util;
import com.wangard.bakra.Utils.Utilities;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPosts extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, AdapterAnimals.AnimalCallBack {

    private Toolbar toolbar;
    private SwipeRefreshLayout refresher;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ArrayList<AnimalItem> list;
    private AdapterAnimals adapter;
    private ViewGroup llNothing;
    private Button btnRefresh;
    private ImageButton ibReTry;
    private int currentPage = 1, lastPage = 0;
    private Boolean isLoading = false, isRefresh = false;

    private SellerData sellerData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_posts);

        initial();
    }

    private void initial() {
        sellerData = SnappyDBUtil.getObject(Constants.USER_DATA, SellerData.class);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        refresher = findViewById(R.id.refresher);
        refresher.setOnRefreshListener(this);

//        nothing to show and refresh layout
        llNothing = findViewById(R.id.llNothing);
        btnRefresh = findViewById(R.id.btnRefresh);
        btnRefresh.setOnClickListener(this);
        ibReTry = findViewById(R.id.ibReTry);
        ibReTry.setOnClickListener(this);

//        recyclerView and pagination
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1)) {
                    loadMore();
                }
            }
        });
        list = new ArrayList<>();
        adapter = new AdapterAnimals(list, this, true);
        recyclerView.setAdapter(adapter);
        progressBar = findViewById(R.id.progressBar);

        refresher.setRefreshing(true);
        getMyPosts();
    }


    private void loadMore() {
        if (currentPage < lastPage & isLoading == false) {
            progressBar.setVisibility(View.VISIBLE);
            currentPage = currentPage + 1;
            getMyPosts();
        }
    }

    private void getMyPosts() {
        if (!Util.isConnectingToInternet(getApplicationContext())) {
            isLoading = false;
            refresher.setRefreshing(false);
            progressBar.setVisibility(View.GONE);
            ibReTry.setVisibility(View.VISIBLE);
            Util.showToastMsg(getApplicationContext(), Constants.kStringNetworkConnectivityError);
            return;
        }
        isLoading = true;
        Call<AnimalsResponse> call = AppController.getInstance().getApiService().getMyPostsResponse(currentPage, sellerData.getId());
        call.enqueue(new Callback<AnimalsResponse>() {
            @Override
            public void onResponse(Call<AnimalsResponse> call, Response<AnimalsResponse> response) {
                isLoading = false;
                refresher.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                ibReTry.setVisibility(View.GONE);
                if (isRefresh) {
                    list.clear();
                    isRefresh = false;
                }
                try {
                    currentPage = response.body().getAnimalsData().getCurrentPage();
                    lastPage = response.body().getAnimalsData().getLastPage();
                    if (response.body().isStatus()) {
                        list.addAll(response.body().getAnimalsData().getAnimalList());
                        adapter.notifyDataSetChanged();
                    } else if (list.size() > 1) {
                        ibReTry.setVisibility(View.VISIBLE);
                    } else if (list.size() < 1) {
                        llNothing.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    Util.showToastMsg(getApplicationContext(), Constants.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AnimalsResponse> call, Throwable t) {
                isLoading = false;
                refresher.setRefreshing(false);
                ibReTry.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                if (list.size() > 1) {
                    ibReTry.setVisibility(View.VISIBLE);
                } else if (list.size() < 1) {
                    llNothing.setVisibility(View.VISIBLE);
                }
                Util.showToastMsg(getApplicationContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        currentPage = 1;
        getMyPosts();
    }

    @Override
    public void onImageClicked(int position) {

        Intent intent = new Intent(MyPosts.this, PostDetails.class);
        intent.putExtra("postItem", list.get(position));
//        intent.putExtra("imagesList", list.get(position).getImages());
        startActivity(intent);
    }

    @Override
    public void onPhoneClicked(int position) {
//        if (checkPhonePermission()) {
//            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + list.get(position).getOwnerContact()));
//            startActivity(intent);
//        }
    }

    @Override
    public void onOptionDelete(int position)
    {
        deletePost(position);
    }

    @Override
    public void onOptionEdit(int position)
    {
        Intent intent = new Intent(MyPosts.this, PostAdd.class);
        intent.putExtra("isEdit", true);
        intent.putExtra("animalItem", list.get(position));
        startActivity(intent);
    }


    private void deletePost(final int position) {
        if (!Util.isConnectingToInternet(this)) {
            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
            return;
        }
        final ProgressDialog dialog = ProgressDialog.show(MyPosts.this, "Delete your Post", "Please wait...", true);
        dialog.setCancelable(false);
        Call<JsonObject> call = AppController.getInstance().getApiService().getDeletePostResponse(list.get(position).getId());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dialog.dismiss();
                try {
                    Util.showToastMsg(getApplicationContext(), response.body().get("message").getAsString());
                    if (response.body().get("status").getAsBoolean())
                    {
                        list.remove(position);
                        adapter.notifyDataSetChanged();
                        return;
                    }
                } catch (Exception e) {
                    Util.showToastMsg(getApplicationContext(), Constants.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dialog.dismiss();
                Util.showToastMsg(getApplicationContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRefresh: {
                llNothing.setVisibility(View.GONE);
                refresher.setRefreshing(true);
                getMyPosts();
                break;
            }
            case R.id.ibReTry: {
                ibReTry.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                getMyPosts();
                break;
            }
        }
    }
}
