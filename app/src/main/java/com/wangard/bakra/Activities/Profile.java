package com.wangard.bakra.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.service.autofill.UserData;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.wangard.bakra.Activities.Auth.ButcherSignUp;
import com.wangard.bakra.Activities.Auth.Login;
import com.wangard.bakra.Activities.Auth.Splash;
import com.wangard.bakra.Activities.Auth.ThankYou;
import com.wangard.bakra.AppController;
import com.wangard.bakra.Models.Seller.SellerData;
import com.wangard.bakra.Models.Seller.SellerResponse;
import com.wangard.bakra.R;
import com.wangard.bakra.Utils.Constants;
import com.wangard.bakra.Utils.SnappyDBUtil;
import com.wangard.bakra.Utils.Util;
import com.wangard.bakra.Utils.Utilities;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import java.io.File;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Profile extends AppCompatActivity implements View.OnClickListener {

    private EditText etName, etEmail, etPhone, etArea;
    private TextView tvName, tvEmail, tvPhone, tvArea, tvLogout, tvMyAds;
    private Button btnUpdate;
    private ViewGroup llEditProfile, llShowProfile, llMyAds;
    private SellerData sellerData;
    private CircleImageView imgUser;
    private View bgProgressBar;
    private ImageButton ibAdd, ibRetry, ibEdit;
    private ProgressBar progressBar;
    private File userImageFile;
    private String name, email, area, phone, image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        sellerData = SnappyDBUtil.getObject(Constants.USER_DATA, SellerData.class);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgUser = findViewById(R.id.imgUser);
        bgProgressBar = findViewById(R.id.bgProgressBar);
        progressBar = findViewById(R.id.progressBar);


//        ImageView
        imgUser = findViewById(R.id.imgUser);
//        EditText
        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        etPhone = findViewById(R.id.etPhone);
        etArea = findViewById(R.id.etArea);
//        TextView
        tvName = findViewById(R.id.tvName);
        tvEmail = findViewById(R.id.tvEmail);
        tvPhone = findViewById(R.id.tvPhone);
        tvArea = findViewById(R.id.tvArea);
        tvLogout = findViewById(R.id.tvLogout);
        tvLogout.setOnClickListener(this);
        tvMyAds = findViewById(R.id.tvMyAds);
//        Button
        btnUpdate = findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(this);
        ibEdit = findViewById(R.id.ibEdit);
        ibEdit.setOnClickListener(this);
        ibAdd = findViewById(R.id.ibAdd);
        ibAdd.setOnClickListener(this);
        ibRetry = findViewById(R.id.ibRetry);
        ibRetry.setOnClickListener(this);
//        Layouts
        llEditProfile = findViewById(R.id.llEditProfile);
        llShowProfile = findViewById(R.id.llShowProfile);
        llMyAds = findViewById(R.id.llMyAds);
        llMyAds.setOnClickListener(this);

        populateData();

        getMyAdsCount();

    }

    @Override
    public void onBackPressed() {

        setResult(RESULT_CANCELED);
        finish();
    }

    private void populateData() {
        etName.setText(sellerData.getName());
        etEmail.setText(sellerData.getEmail());
        etPhone.setText(sellerData.getPhoneNumber());
        etArea.setText(sellerData.getArea());

        tvName.setText(sellerData.getName());
        tvEmail.setText(sellerData.getEmail());
        tvPhone.setText(sellerData.getPhoneNumber());
        tvArea.setText(sellerData.getArea());

        if (sellerData.getImage().contains("http"))
            Picasso.get().load(sellerData.getImage()).error(R.drawable.ic_user_placehlder).placeholder(R.drawable.ic_user_placehlder).into(imgUser);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibAdd: {
                openCameraOrGallery();
                break;
            }
            case R.id.ibRetry: {
                progressBar.setVisibility(View.VISIBLE);
                ibRetry.setVisibility(View.GONE);
                uploadImage();
                break;
            }
            case R.id.ibEdit: {
                llShowProfile.setVisibility(View.GONE);
                llEditProfile.setVisibility(View.VISIBLE);
                ibEdit.setVisibility(View.GONE);
                etName.requestFocus();
                break;
            }
            case R.id.btnUpdate: {
                if (validation())
                    updateProfile();
                break;
            }
            case R.id.tvLogout: {
                Utilities.getInstance(Profile.this).clearAllSharedPreferences();
                SnappyDBUtil.deleteObjectOrList(Constants.USER_DATA);
                startActivity(new Intent(Profile.this, Splash.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
                break;
            }
            case R.id.llMyAds: {
                startActivity(new Intent(Profile.this, MyPosts.class));
                break;
            }
        }
    }


    private void openCameraOrGallery() {
        new ImagePicker.Builder(Profile.this)
                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.PNG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(false)
                .allowOnlineImages(false)
                .build();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
            userImageFile = new File(mPaths.get(0));
            Picasso.get().load(userImageFile).into(imgUser);
            uploadImage();
        }
    }

    private void updateProfile() {
        if (!Util.isConnectingToInternet(this)) {
            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
            return;
        }
        final ProgressDialog dialog = ProgressDialog.show(Profile.this, "Updating your profile", "Please wait...", true);
        dialog.setCancelable(false);
        Call<SellerResponse> call = AppController.getInstance().getApiService().getSellerUpdateResponse(sellerData.getId(), name, email, area, phone, image);
        call.enqueue(new Callback<SellerResponse>() {
            @Override
            public void onResponse(Call<SellerResponse> call, Response<SellerResponse> response) {
                dialog.dismiss();
                try {
                    Util.showToastMsg(getApplicationContext(), response.body().getMessage());
                    if (response.body().isStatus()) {

                        SnappyDBUtil.saveObject(Constants.USER_DATA, response.body().getSellerData());
                        setResult(RESULT_OK);
                        finish();
                        return;
                    }
                } catch (Exception e) {
                    Util.showToastMsg(getApplicationContext(), Constants.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SellerResponse> call, Throwable t) {
                dialog.dismiss();
                Util.showToastMsg(getApplicationContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    private void getMyAdsCount() {
        if (!Util.isConnectingToInternet(this)) {
            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
            return;
        }
        Call<JsonObject> call = AppController.getInstance().getApiService().getMyPostsCountResponse(sellerData.getId());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body().get("status").getAsBoolean()) {
                        tvMyAds.setText(response.body().get("count").getAsInt() + "");
                        return;
                    }
                } catch (Exception e) {
                    Util.showToastMsg(getApplicationContext(), Constants.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Util.showToastMsg(getApplicationContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    private void uploadImage() {
        if (!validation())
            return;
        ibAdd.setEnabled(false);
        bgProgressBar.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        Ion.with(getApplicationContext()).load(Constants.BASE_URL + "utils/upload")
                .setMultipartFile("file", userImageFile)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        ibAdd.setEnabled(true);
                        progressBar.setVisibility(View.GONE);
                        if (e == null) {
                            try {
                                if (result.contains("http")) {
                                    bgProgressBar.setVisibility(View.GONE);
                                    image = result;
                                    updateProfile();
                                } else {
                                    ibRetry.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e1) {
                                ibRetry.setVisibility(View.VISIBLE);
                            }
                            return;
                        }
                        ibRetry.setVisibility(View.VISIBLE);
                    }
                });
    }

    private boolean validation() {
        name = etName.getText().toString();
        email = etEmail.getText().toString();
        area = etArea.getText().toString();
        phone = etPhone.getText().toString();

        if (name.isEmpty()) {

            etName.setError("Missing username");
            etName.requestFocus();
            return false;
        }
        if (area.isEmpty()) {
            etArea.setError("Missing area name");
            etArea.requestFocus();
            return false;
        }
        if (!Util.isValidMobile(phone)) {
            etPhone.setError("Valid phone number required");
            etPhone.requestFocus();
            return false;
        }

        return true;
    }
}
