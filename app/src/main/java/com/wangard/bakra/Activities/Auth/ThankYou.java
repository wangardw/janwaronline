package com.wangard.bakra.Activities.Auth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.wangard.bakra.Activities.DashBoard;
import com.wangard.bakra.R;

public class ThankYou extends AppCompatActivity implements View.OnClickListener {

    private TextView tvButcherSignUp, tvPurchase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);

        initial();
    }

    private void initial() {
        tvButcherSignUp = findViewById(R.id.tvButcherSignUp);
        tvButcherSignUp.setOnClickListener(this);
        tvPurchase = findViewById(R.id.tvPurchase);
        tvPurchase.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.tvButcherSignUp:
            {
                startActivity(new Intent(ThankYou.this, ButcherSignUp.class).putExtra("isWelcome", false));
                finish();
                break;
            }
            case R.id.tvPurchase:
            {
                startActivity(new Intent(ThankYou.this, DashBoard.class));
                finish();
                break;
            }
        }
    }
}
