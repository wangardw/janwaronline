package com.wangard.bakra.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.wangard.bakra.Activities.Auth.ButcherSignUp;
import com.wangard.bakra.Activities.Auth.Login;
import com.wangard.bakra.AppController;
import com.wangard.bakra.Models.Animals.AnimalItem;
import com.wangard.bakra.Models.Animals.ImagesItem;
import com.wangard.bakra.Models.Seller.SellerData;
import com.wangard.bakra.R;
import com.wangard.bakra.Utils.Constants;
import com.wangard.bakra.Utils.SnappyDBUtil;
import com.wangard.bakra.Utils.Util;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostAdd extends AppCompatActivity implements View.OnClickListener {

    private EditText etOwnerName, etPhoneNum, etPrice, etWeight, etLocation, etRemarks;
    private String ownerName, ownerPhoneNum, animalPrice, animalWeight, ownerLocation, animalRemarks, is_featured = "0", category_id, animalTeeth;
    private int rCategoryID, rTeethID;
    private RadioGroup rgAnimalCategory, rgAnimalTeeth;
    private RadioButton rSelectedCategory, rSelectedTeeth;

    private Button btnAddAnimal;
    private SellerData sellerData;
    private SelectedImage selectedImage;
    private ImageView imgHeader, imgAnimalOne, imgAnimalTwo, imgAnimalThree, imgAnimalFour;
    private View bgProgressBarOne, bgProgressBarTwo, bgProgressBarThree, bgProgressBarFour;
    private ViewGroup llOwnerDetails;
    private ProgressBar progressBarOne, progressBarTwo, progressBarThree, progressBarFour;
    private ImageButton ibEdit, ibRetryOne, ibRetryTwo, ibRetryThree, ibRetryFour;
    private String[] imagesArray = new String[4];
    private File[] filesArray = new File[4];
    private boolean isMinimumImage = false, isEdit = false;
    private AnimalItem animalItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_add);

        initial();
    }

    private void initial() {
        isEdit = getIntent().getExtras().getBoolean("isEdit");
        if (isEdit)
            animalItem = (AnimalItem) getIntent().getSerializableExtra("animalItem");
        sellerData = SnappyDBUtil.getObject(Constants.USER_DATA, SellerData.class);

        etOwnerName = findViewById(R.id.etOwnerName);
        etPhoneNum = findViewById(R.id.etPhoneNum);
        etPrice = findViewById(R.id.etPrice);
        etWeight = findViewById(R.id.etWeight);
        etLocation = findViewById(R.id.etLocation);
        etRemarks = findViewById(R.id.etRemarks);

        rgAnimalCategory = findViewById(R.id.rgAnimalCategory);
        rgAnimalTeeth = findViewById(R.id.rgAnimalTeeth);

        btnAddAnimal = findViewById(R.id.btnAddAnimal);
        btnAddAnimal.setOnClickListener(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        llOwnerDetails = findViewById(R.id.llOwnerDetails);
//        ImageView
        imgHeader = findViewById(R.id.imgHeader);
        imgAnimalOne = findViewById(R.id.imgAnimalOne);
        imgAnimalOne.setOnClickListener(this);
        imgAnimalTwo = findViewById(R.id.imgAnimalTwo);
        imgAnimalTwo.setOnClickListener(this);
        imgAnimalThree = findViewById(R.id.imgAnimalThree);
        imgAnimalThree.setOnClickListener(this);
        imgAnimalFour = findViewById(R.id.imgAnimalFour);
        imgAnimalFour.setOnClickListener(this);

//        ProgressBar backgrounds
        bgProgressBarOne = findViewById(R.id.bgProgressBarOne);
        bgProgressBarTwo = findViewById(R.id.bgProgressBarTwo);
        bgProgressBarThree = findViewById(R.id.bgProgressBarThree);
        bgProgressBarFour = findViewById(R.id.bgProgressBarFour);

//        ProgressBar
        progressBarOne = findViewById(R.id.progressBarOne);
        progressBarTwo = findViewById(R.id.progressBarTwo);
        progressBarThree = findViewById(R.id.progressBarThree);
        progressBarFour = findViewById(R.id.progressBarFour);

//        ImageButton
        ibRetryOne = findViewById(R.id.ibRetryOne);
        ibRetryOne.setOnClickListener(this);
        ibRetryTwo = findViewById(R.id.ibRetryTwo);
        ibRetryTwo.setOnClickListener(this);
        ibRetryThree = findViewById(R.id.ibRetryThree);
        ibRetryThree.setOnClickListener(this);
        ibRetryFour = findViewById(R.id.ibRetryFour);
        ibRetryFour.setOnClickListener(this);
        ibEdit = findViewById(R.id.ibEdit);
        ibEdit.setOnClickListener(this);

        if (sellerData.getType().equalsIgnoreCase("seller")) {
            llOwnerDetails.setVisibility(View.GONE);
        }

        if (animalItem != null)
            populateDataEditPostData();

    }

    private void populateDataEditPostData() {
        ArrayList<ImagesItem> imagesList = animalItem.getImages();
        for (int i = 0; i < animalItem.getImages().size(); i++) {
            imagesArray[i] = imagesList.get(i).getImageUrl();
            if (i == 0)
            {
                Picasso.get().load(imagesList.get(i).getImageUrl()).error(R.drawable.ic_img_placeholder).placeholder(R.drawable.ic_img_placeholder).fit().centerCrop().into(imgHeader);
                Picasso.get().load(imagesList.get(i).getImageUrl()).error(R.drawable.ic_img_placeholder).placeholder(R.drawable.ic_img_placeholder).fit().centerCrop().into(imgAnimalOne);
            }
            if (i == 1)
                Picasso.get().load(imagesList.get(i).getImageUrl()).error(R.drawable.ic_img_placeholder).placeholder(R.drawable.ic_img_placeholder).fit().centerCrop().into(imgAnimalTwo);
            if (i == 2)
                Picasso.get().load(imagesList.get(i).getImageUrl()).error(R.drawable.ic_img_placeholder).placeholder(R.drawable.ic_img_placeholder).fit().centerCrop().into(imgAnimalThree);
            if (i == 3)
                Picasso.get().load(imagesList.get(i).getImageUrl()).error(R.drawable.ic_img_placeholder).placeholder(R.drawable.ic_img_placeholder).fit().centerCrop().into(imgAnimalFour);
        }
        String categoryName = animalItem.getCategory().getName();
        if (categoryName.equalsIgnoreCase("Bakra")) {
            rgAnimalCategory.check(R.id.rbGoat);
        } else if (categoryName.equalsIgnoreCase("Cow")) {
            rgAnimalCategory.check(R.id.rbCow);
        } else if (categoryName.equalsIgnoreCase("Camel")) {
            rgAnimalCategory.check(R.id.rbCamel);
        }

        String teeth = animalItem.getTeeths();
        if (teeth.equalsIgnoreCase("2")) {
            rgAnimalTeeth.check(R.id.rbTwo);
        } else if (teeth.equalsIgnoreCase("4")) {
            rgAnimalTeeth.check(R.id.rbFour);
        } else if (teeth.equalsIgnoreCase("6")) {
            rgAnimalTeeth.check(R.id.rbSix);
        }

        if (sellerData.getType().equalsIgnoreCase("seller")) {
            llOwnerDetails.setVisibility(View.GONE);
        } else {
            etOwnerName.setText(animalItem.getOwnerName());
            etPhoneNum.setText(animalItem.getOwnerContact());
            etLocation.setText(animalItem.getOwnerArea());
        }

        etPrice.setText(animalItem.getPrice() + "");
        etWeight.setText(animalItem.getWeight());
        etRemarks.setText(animalItem.getRemarks());

        btnAddAnimal.setText("Update");
        isMinimumImage = true;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddAnimal: {
                if (isValidate())
                    performAddAnimalButton();
                break;
            }
            case R.id.imgAnimalOne: {
                if (filesArray[0] != null) {
                    Picasso.get().load(filesArray[0]).fit().centerCrop().into(imgHeader);
                    break;
                } else if (imagesArray[0].contains("http")) {
                    Picasso.get().load(imagesArray[0]).error(R.drawable.ic_img_placeholder).placeholder(R.drawable.ic_img_placeholder).fit().centerCrop().into(imgHeader);
                }
                selectedImage = SelectedImage.ONE;
                openCameraOrGallery();
                break;
            }
            case R.id.imgAnimalTwo: {
                if (filesArray[1] != null) {
                    Picasso.get().load(filesArray[1]).fit().centerCrop().into(imgHeader);
                    break;
                } else if (imagesArray[1].contains("http")) {
                    Picasso.get().load(imagesArray[1]).error(R.drawable.ic_img_placeholder).placeholder(R.drawable.ic_img_placeholder).fit().centerCrop().into(imgHeader);
                }
                selectedImage = SelectedImage.TWO;
                openCameraOrGallery();
                break;
            }
            case R.id.imgAnimalThree: {
                if (filesArray[2] != null) {
                    Picasso.get().load(filesArray[2]).fit().centerCrop().into(imgHeader);
                    break;
                } else if (imagesArray[2].contains("http")) {
                    Picasso.get().load(imagesArray[2]).error(R.drawable.ic_img_placeholder).placeholder(R.drawable.ic_img_placeholder).fit().centerCrop().into(imgHeader);
                }
                selectedImage = SelectedImage.THREE;
                openCameraOrGallery();
                break;
            }
            case R.id.imgAnimalFour: {
                if (filesArray[3] != null) {
                    Picasso.get().load(filesArray[3]).fit().centerCrop().into(imgHeader);
                    break;
                } else if (imagesArray[3].contains("http")) {
                    Picasso.get().load(imagesArray[3]).error(R.drawable.ic_img_placeholder).placeholder(R.drawable.ic_img_placeholder).fit().centerCrop().into(imgHeader);
                }
                selectedImage = SelectedImage.FOUR;
                openCameraOrGallery();
                break;
            }

            case R.id.ibRetryOne: {
                uploadImageOne();
                break;
            }
            case R.id.ibRetryTwo: {
                uploadImageTwo();
                break;
            }
            case R.id.ibRetryThree: {
                uploadImageThree();
                break;
            }
            case R.id.ibRetryFour: {
                uploadImageFour();
                break;
            }
            case R.id.ibEdit: {
                openCameraOrGallery();
                break;
            }
        }
    }

    private void performAddAnimalButton() {
        if (btnAddAnimal.getText().toString().equalsIgnoreCase("Add")) {
            addAnimal();
            return;
        }
        updateAnimal();
    }

    private void updateAnimal() {
        if (imagesArray.length < 1) {
            Util.showToastMsg(PostAdd.this, "Minimum one image required");
            return;
        }
        if (!Util.isConnectingToInternet(this)) {
            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
            return;
        }
        final ProgressDialog dialog = ProgressDialog.show(PostAdd.this, "Update your animal", "Please wait...", true);
        dialog.setCancelable(false);
        Call<JsonObject> postAnimal = AppController.getInstance().getApiService().getUpdateAnimalResponse(animalItem.getId(), sellerData.getId(), ownerLocation, animalTeeth, animalPrice, animalWeight, category_id, animalRemarks, is_featured, ownerPhoneNum, ownerName, imagesArray);
        postAnimal.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dialog.dismiss();
                Util.showToastMsg(PostAdd.this, "Animal Updated successfully");
                finish();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
            addImageToArrayAndUpload(new File(mPaths.get(0)));
        }
    }

    private void addImageToArrayAndUpload(File file) {
        if (selectedImage == SelectedImage.ONE) {
            filesArray[0] = file;
            Picasso.get().load(file).fit().centerCrop().into(imgAnimalOne);
            Picasso.get().load(file).fit().centerCrop().into(imgHeader);
            uploadImageOne();
        } else if (selectedImage == SelectedImage.TWO) {
            filesArray[1] = file;
            Picasso.get().load(file).fit().centerCrop().into(imgAnimalTwo);
            Picasso.get().load(file).fit().centerCrop().into(imgHeader);
            uploadImageTwo();
        } else if (selectedImage == SelectedImage.THREE) {
            filesArray[2] = file;
            Picasso.get().load(file).fit().centerCrop().into(imgAnimalThree);
            Picasso.get().load(file).fit().centerCrop().into(imgHeader);
            uploadImageThree();
        } else if (selectedImage == SelectedImage.FOUR) {
            filesArray[3] = file;
            Picasso.get().load(file).fit().centerCrop().into(imgAnimalFour);
            Picasso.get().load(file).fit().centerCrop().into(imgHeader);
            uploadImageFour();
        }
    }

    private void openCameraOrGallery() {
        new ImagePicker.Builder(PostAdd.this)
                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.PNG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(false)
                .allowOnlineImages(false)
                .build();
    }


    private void uploadImageOne() {
        bgProgressBarOne.setVisibility(View.VISIBLE);
        progressBarOne.setVisibility(View.VISIBLE);
        Ion.with(getApplicationContext()).load(Constants.BASE_URL + "utils/upload")
                .setMultipartFile("file", filesArray[0])
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        try {

                            if (e != null) {
                                progressBarOne.setVisibility(View.GONE);
                                ibRetryOne.setVisibility(View.VISIBLE);
                            }
                            if (result.contains("http")) {
                                progressBarOne.setVisibility(View.GONE);
                                bgProgressBarOne.setVisibility(View.GONE);
                                imagesArray[0] = result;
                                isMinimumImage = true;
                                return;
                            } else {
                                progressBarOne.setVisibility(View.GONE);
                                ibRetryOne.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e1) {

                        }
                    }
                });
    }

    private void uploadImageTwo() {
        bgProgressBarTwo.setVisibility(View.VISIBLE);
        progressBarTwo.setVisibility(View.VISIBLE);
        Ion.with(getApplicationContext()).load(Constants.BASE_URL + "utils/upload")
                .setMultipartFile("file", filesArray[1])
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        try {

                            if (e != null) {
                                progressBarTwo.setVisibility(View.GONE);
                                ibRetryTwo.setVisibility(View.VISIBLE);
                            }
                            if (result.contains("http")) {
                                progressBarTwo.setVisibility(View.GONE);
                                bgProgressBarTwo.setVisibility(View.GONE);
                                imagesArray[1] = result;
                                isMinimumImage = true;
                                return;
                            } else {
                                progressBarTwo.setVisibility(View.GONE);
                                ibRetryTwo.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e1) {
                            progressBarTwo.setVisibility(View.GONE);
                            ibRetryTwo.setVisibility(View.VISIBLE);
                        }
                    }
                });
    }

    private void uploadImageThree() {
        bgProgressBarThree.setVisibility(View.VISIBLE);
        progressBarThree.setVisibility(View.VISIBLE);
        Ion.with(getApplicationContext()).load(Constants.BASE_URL + "utils/upload")
                .setMultipartFile("file", filesArray[2])
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        try {

                            if (e != null) {
                                progressBarThree.setVisibility(View.GONE);
                                ibRetryThree.setVisibility(View.VISIBLE);
                            }
                            if (result.contains("http")) {
                                progressBarThree.setVisibility(View.GONE);
                                bgProgressBarThree.setVisibility(View.GONE);
                                imagesArray[2] = result;
                                isMinimumImage = true;
                                return;
                            } else {
                                progressBarThree.setVisibility(View.GONE);
                                ibRetryThree.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e1) {
                            progressBarThree.setVisibility(View.GONE);
                            ibRetryThree.setVisibility(View.VISIBLE);
                        }
                    }
                });
    }

    private void uploadImageFour() {
        bgProgressBarFour.setVisibility(View.VISIBLE);
        progressBarFour.setVisibility(View.VISIBLE);
        Ion.with(getApplicationContext()).load(Constants.BASE_URL + "utils/upload")
                .setMultipartFile("file", filesArray[3])
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        try {

                            if (e != null) {
                                progressBarFour.setVisibility(View.GONE);
                                ibRetryFour.setVisibility(View.VISIBLE);
                            }
                            if (result.contains("http")) {
                                progressBarFour.setVisibility(View.GONE);
                                bgProgressBarFour.setVisibility(View.GONE);
                                imagesArray[3] = result;
                                isMinimumImage = true;
                                return;
                            } else {
                                progressBarFour.setVisibility(View.GONE);
                                ibRetryFour.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e1) {
                            progressBarFour.setVisibility(View.GONE);
                            ibRetryFour.setVisibility(View.VISIBLE);
                        }
                    }
                });
    }


    private void addAnimal() {
        if (imagesArray.length < 1) {
            Util.showToastMsg(PostAdd.this, "Minimum one image required");
            return;
        }
        if (!Util.isConnectingToInternet(this)) {
            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
            return;
        }
        final ProgressDialog dialog = ProgressDialog.show(PostAdd.this, "Posting new ad", "Please wait...", true);
        dialog.setCancelable(false);
        Call<JsonObject> postAnimal = AppController.getInstance().getApiService().getAddAnimalResponse(sellerData.getId(), ownerLocation, animalTeeth, animalPrice, animalWeight, category_id, animalRemarks, is_featured, ownerPhoneNum, ownerName, imagesArray);
        postAnimal.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dialog.dismiss();
                Util.showToastMsg(PostAdd.this, "Animal added successfully");
                finish();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                dialog.dismiss();
            }
        });
    }

    public boolean isValidate() {

        rCategoryID = rgAnimalCategory.getCheckedRadioButtonId();
        rTeethID = rgAnimalTeeth.getCheckedRadioButtonId();

        ownerName = etOwnerName.getText().toString();
        ownerPhoneNum = etPhoneNum.getText().toString();
        animalPrice = etPrice.getText().toString();
        animalWeight = etWeight.getText().toString();
        ownerLocation = etLocation.getText().toString();
        animalRemarks = etRemarks.getText().toString();

        if (!isMinimumImage) {
            Util.showToastMsg(PostAdd.this, "Please add minimum one image");
            return false;
        }
        if (rCategoryID == -1) {
            Util.showToastMsg(this, "Select animal category");
            return false;
        } else {
            rSelectedCategory = rgAnimalCategory.findViewById(rCategoryID);
            category_id = rSelectedCategory.getText().toString();

            if (category_id.equals("Goat/Sheep")) {
                category_id = "1";
            } else if (category_id.equals("Cow/Bull")) {
                category_id = "2";
            } else if (category_id.equals("Camel")) {
                category_id = "3";
            } else {
                Util.showToastMsg(this, "Invalid category");
            }
        }


        if (rTeethID == -1) {
            Util.showToastMsg(this, "Select animal teeth");
            return false;
        } else {
            rSelectedTeeth = rgAnimalTeeth.findViewById(rTeethID);
            animalTeeth = rSelectedTeeth.getText().toString();
        }

        if (animalPrice.isEmpty()) {
            etPrice.setError("Animal price missing");
            etPrice.requestFocus();
            return false;
        }
        if (animalWeight.isEmpty()) {
            etWeight.setError("Animal weight missing");
            etWeight.requestFocus();
            return false;
        }
        if (sellerData.getType().equalsIgnoreCase("seller")) {
            ownerName = sellerData.getName();
            ownerPhoneNum = sellerData.getPhoneNumber();
            ownerLocation = sellerData.getArea();
            return true;
        }
        if (ownerName.isEmpty()) {
            etOwnerName.setError("Owner name missing");
            etOwnerName.requestFocus();
            return false;
        }
        if (ownerPhoneNum.isEmpty()) {
            etPhoneNum.setError("Contact number missing");
            etPhoneNum.requestFocus();
            return false;
        }
        if (ownerLocation.isEmpty()) {
            etLocation.setError("Location missing");
            etLocation.requestFocus();
            return false;
        }

        return true;
    }

    private enum SelectedImage {
        ONE, TWO, THREE, FOUR
    }
}
