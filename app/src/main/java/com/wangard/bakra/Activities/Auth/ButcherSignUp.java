package com.wangard.bakra.Activities.Auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.wangard.bakra.AppController;
import com.wangard.bakra.R;
import com.wangard.bakra.Utils.Constants;
import com.wangard.bakra.Utils.Util;
import com.wangard.bakra.Utils.Utilities;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import java.io.File;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ButcherSignUp extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private Spinner spnLocation;
    private CircleImageView imgUser;
    private View bgProgressBar;
    private ImageButton btnBack, ibAdd, ibRetry;
    private ProgressBar progressBar;
    private File butcherImageFile;
    private Button btnSignUp;
    private CheckBox cbGoat, cbCow, cbCamel;
    private TextView tvPhone;
    private EditText etName, etArea, etGoat, etCow, etCamel;
    private String name, phone, area, goatPrice = "", cowPrice = "", camelPrice = "", imagePath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_butcher_sign_up);

        initial();
    }

    private void initial() {

        imgUser = findViewById(R.id.imgUser);
        bgProgressBar = findViewById(R.id.bgProgressBar);
        progressBar = findViewById(R.id.progressBar);

        ibAdd = findViewById(R.id.ibAdd);
        ibAdd.setOnClickListener(this);
        ibRetry = findViewById(R.id.ibRetry);
        ibRetry.setOnClickListener(this);

        etName = findViewById(R.id.etName);
        etArea = findViewById(R.id.etArea);
        etGoat = findViewById(R.id.etGoat);
        etCow = findViewById(R.id.etCow);
        etCamel = findViewById(R.id.etCamel);

        cbGoat = findViewById(R.id.cbGoat);
        cbGoat.setOnCheckedChangeListener(this);
        cbCow = findViewById(R.id.cbCow);
        cbCow.setOnCheckedChangeListener(this);
        cbCamel = findViewById(R.id.cbCamel);
        cbCamel.setOnCheckedChangeListener(this);

        tvPhone = findViewById(R.id.tvPhone);
        tvPhone.setOnClickListener(this);

        btnSignUp = findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(this);
        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        spnLocation = findViewById(R.id.spnLocation);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ButcherSignUp.this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.locationsArray));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnLocation.setAdapter(adapter);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnSignUp: {
                if (validation())
                    signUpButcher();
                break;
            }
            case R.id.btnBack: {
                finish();
                break;
            }
            case R.id.ibAdd: {
                openCameraOrGallery();
                break;
            }
            case R.id.ibRetry: {
                progressBar.setVisibility(View.VISIBLE);
                ibRetry.setVisibility(View.GONE);
                uploadImage();
                break;
            }
            case R.id.tvPhone: {
                tvPhone.setError(null);
                tvPhone.setText("");
                openAccountKit();
                break;
            }
        }
    }

    private void getPhoneNumber() {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                String accountKitId = account.getId();
                PhoneNumber phoneNumber = account.getPhoneNumber();
                if (phoneNumber != null) {
                    String phoneNumberString = phoneNumber.toString();
                    tvPhone.setText(phoneNumberString);
                }
            }

            @Override
            public void onError(final AccountKitError error) {
                tvPhone.setError(error.getUserFacingMessage());
            }
        });
    }

    private void openAccountKit() {
        final Intent intent = new Intent(ButcherSignUp.this, AccountKitActivity.class);

        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder = new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE, AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, 102);
    }

    private void openCameraOrGallery() {
        new ImagePicker.Builder(ButcherSignUp.this)
                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.PNG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(false)
                .allowOnlineImages(false)
                .build();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
            butcherImageFile = new File(mPaths.get(0));
            Picasso.get().load(butcherImageFile).into(imgUser);
            uploadImage();
        }
        if (requestCode == 102) { // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            if (loginResult.getError() != null) {
                if (loginResult.getError().getErrorType().getMessage() != null)
                    tvPhone.setError(loginResult.getError().getErrorType().getMessage());
            } else if (loginResult.wasCancelled()) {
                tvPhone.setError("Number verification cancel");
            } else {
                getPhoneNumber();
            }
        }
    }

    private void signUpButcher() {
        if (!Util.isConnectingToInternet(this)) {
            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
            return;
        }
        final ProgressDialog dialog = ProgressDialog.show(ButcherSignUp.this, "Registering new butcher", "Please wait...", true);
        dialog.setCancelable(false);
        Call<JsonObject> call = AppController.getInstance().getApiService().getButcherSignUpResponse(name, phone, area, imagePath, getJsonPrices());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dialog.dismiss();
                try {
                    Util.showToastMsg(getApplicationContext(), response.body().get("message").getAsString());
                    if (response.body().get("status").getAsBoolean()) {
                        if (getIntent().getExtras().getBoolean("isWelcome")) {
                            Utilities.getInstance(getApplicationContext()).saveIntegerPreferences(Constants.PREF_USER_STATE, 1);
                            startActivity(new Intent(ButcherSignUp.this, ThankYou.class));
                            finish();
                        } else {
                            finish();
                        }
                        return;
                    }
                } catch (Exception e) {
                    Util.showToastMsg(getApplicationContext(), Constants.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dialog.dismiss();
                Util.showToastMsg(getApplicationContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    private String getJsonPrices() {
        JsonArray array = new JsonArray();

        if (cbGoat.isChecked()) {
            JsonObject object = new JsonObject();
            object.addProperty("category_id", 1);
            object.addProperty("price", goatPrice);
            array.add(object);
        }
        if (cbCow.isChecked()) {
            JsonObject object = new JsonObject();
            object.addProperty("category_id", 2);
            object.addProperty("price", cowPrice);
            array.add(object);
        }
        if (cbCamel.isChecked()) {
            JsonObject object = new JsonObject();
            object.addProperty("category_id", 3);
            object.addProperty("price", camelPrice);
            array.add(object);
        }
        return array.toString();
    }


    private void uploadImage() {
        ibAdd.setEnabled(false);
        bgProgressBar.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        Ion.with(getApplicationContext()).load(Constants.BASE_URL + "utils/upload")
                .setMultipartFile("file", butcherImageFile)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        ibAdd.setEnabled(true);
                        progressBar.setVisibility(View.GONE);
                        if (e == null) {
                            try {
                                if (result.contains("http")) {
                                    bgProgressBar.setVisibility(View.GONE);
                                    imagePath = result;
                                } else {
                                    ibRetry.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e1) {
                                ibRetry.setVisibility(View.VISIBLE);
                            }
                            return;
                        }
                        ibRetry.setVisibility(View.VISIBLE);
                    }
                });
    }

    private boolean validation() {

        name = etName.getText().toString();
        phone = tvPhone.getText().toString();
        area = spnLocation.getSelectedItem().toString();
        goatPrice = etGoat.getText().toString();
        cowPrice = etCow.getText().toString();
        camelPrice = etCamel.getText().toString();


        if (name.isEmpty()) {

            etName.setError("Missing Name");
            etName.requestFocus();
            return false;
        }
        if (!Util.isValidMobile(phone)) {
            tvPhone.setError("Valid phone number required");
            tvPhone.requestFocus();
            return false;
        }
        if (area.isEmpty()) {

            etArea.setError("Missing area name");
            etArea.requestFocus();
            return false;
        }
        if (cbGoat.isChecked()) {
            if (goatPrice.isEmpty()) {
                etGoat.setError("Empty price");
                etGoat.requestFocus();
                return false;
            }
        }
        if (cbCow.isChecked()) {
            if (cowPrice.isEmpty()) {
                etCow.setError("Empty price");
                etCow.requestFocus();
                return false;
            }
        }
        if (cbCamel.isChecked()) {
            if (camelPrice.isEmpty()) {
                etCamel.setError("Empty price");
                etCamel.requestFocus();
                return false;
            }
        }
        if (!cbGoat.isChecked() && !cbCow.isChecked() && !cbCamel.isChecked()) {
            Util.showToastMsg(ButcherSignUp.this, "Select atleast one price");
            return false;
        }

        if (imagePath.isEmpty()) {
            Util.showToastMsg(ButcherSignUp.this, "Select butcher image");
            return false;
        }
        return true;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.cbGoat) {
            etGoat.setEnabled(isChecked);
        } else if (buttonView.getId() == R.id.cbCow) {
            etCow.setEnabled(isChecked);
        } else if (buttonView.getId() == R.id.cbCamel) {
            etCamel.setEnabled(isChecked);
        }
    }
}
