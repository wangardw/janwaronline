package com.wangard.bakra.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.android.material.navigation.NavigationView;
import com.wangard.bakra.Adapters.AdapterAnimals;
import com.wangard.bakra.Adapters.AdapterAreas;
import com.wangard.bakra.AppController;
import com.wangard.bakra.Models.Animals.AnimalItem;
import com.wangard.bakra.Models.Areas.AreaItem;
import com.wangard.bakra.Models.Areas.AreaResponse;
import com.wangard.bakra.R;
import com.wangard.bakra.Utils.Constants;
import com.wangard.bakra.Utils.Util;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterAreas extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, AdapterAreas.AreasCallback {

    private Toolbar toolbar;
    private MenuItem itemCheck;
    private SwipeRefreshLayout refresher;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ArrayList<AreaItem> list;
    private AdapterAreas adapter;
    private Boolean isLoading = false, isRefresh = false;
    private int currentPage = 1, lastPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_areas);
        init();
    }

    private void init() {

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        refresher = findViewById(R.id.areaRefresher);
        refresher.setOnRefreshListener(this);

        recyclerView = findViewById(R.id.rvAreas);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1)) {
                    loadMore();
                }
            }
        });

        list = new ArrayList<>();
        adapter = new AdapterAreas(list, this);
        recyclerView.setAdapter(adapter);
        progressBar = findViewById(R.id.progressBar);

        refresher.setRefreshing(true);

        getAllAreas();
    }

    private void getAllAreas() {

        if (!Util.isConnectingToInternet(this)) {
            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
            return;
        }
        isLoading = true;
        Call<AreaResponse> call = AppController.getInstance().getApiService().getAreasResponse(currentPage);
        call.enqueue(new Callback<AreaResponse>() {
            @Override
            public void onResponse(Call<AreaResponse> call, Response<AreaResponse> response) {
                isLoading = false;
                refresher.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                if (isRefresh) {
                    list.clear();
                    isRefresh = false;
                }
                try {
                    currentPage = response.body().getData().getCurrent_page();
                    lastPage = response.body().getData().getLast_page();
                    if (response.body().getStatus()) {
                        list.addAll(response.body().getData().getAreaList());
                        adapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    Util.showToastMsg(FilterAreas.this, Constants.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AreaResponse> call, Throwable t) {

            }
        });
    }

    private void loadMore() {
        if (currentPage < lastPage & isLoading == false) {
            progressBar.setVisibility(View.VISIBLE);
            currentPage = currentPage + 1;
            getAllAreas();
        }
    }

    @Override
    public void onRefresh() {
        currentPage = 1;
        isRefresh = true;
        getAllAreas();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter_check, menu);
        itemCheck = menu.findItem(R.id.action_check);
        itemCheck.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_check) {
            Intent intent = getIntent();
            intent.putExtra("areas", getSelectedAreas());
            setResult(RESULT_OK, intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private String getSelectedAreas() {
        String areas = "";
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isSelected()) {
                if (i+1 == list.size()) {
                    areas = areas + list.get(i).getName();
                } else {
                    areas = areas + list.get(i).getName() + ", ";
                }

            }
        }
        return areas;
    }

    @Override
    public void onItemSelectionChange(int position, boolean isChecked) {
        list.get(position).setSelected(isChecked);
        boolean isVisible = false;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isSelected()) {
                isVisible = true;
                break;
            }
        }
        itemCheck.setVisible(isVisible);
    }
}
