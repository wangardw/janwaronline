package com.wangard.bakra.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chahinem.pageindicator.PageIndicator;
import com.wangard.bakra.Adapters.AdapterImages;
import com.wangard.bakra.Models.Animals.AnimalItem;
import com.wangard.bakra.Models.Animals.ImagesItem;
import com.wangard.bakra.R;

import java.util.ArrayList;

public class PostDetails extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AdapterImages adapter;
    private PageIndicator pageIndicator;
    private Toolbar toolbar;
    private TextView tvPrice, tvNumber, tvName, tvWeight, tvTeeth, tvDescription;
    private AnimalItem animalItem;
    private ImageView imgFeatured;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_details);

        initial();

    }

    private void initial() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        pageIndicator = findViewById(R.id.pageIndicator);
        recyclerView = findViewById(R.id.recyclerView);
        SnapHelper snapHelper = new PagerSnapHelper();
        recyclerView.setLayoutManager(new LinearLayoutManager(PostDetails.this, RecyclerView.HORIZONTAL, false));
        snapHelper.attachToRecyclerView(recyclerView);
        animalItem = (AnimalItem) getIntent().getSerializableExtra("postItem");
        ArrayList<ImagesItem> list = animalItem.getImages();
        adapter = new AdapterImages(list);
        recyclerView.setAdapter(adapter);
        pageIndicator.attachTo(recyclerView);

        tvPrice = findViewById(R.id.tvPrice);
        tvNumber = findViewById(R.id.tvNumber);
        tvName = findViewById(R.id.tvName);
        tvWeight = findViewById(R.id.tvWeight);
        tvTeeth = findViewById(R.id.tvTeeth);
        tvDescription = findViewById(R.id.tvDescription);

        imgFeatured = findViewById(R.id.imgFeatured);

        populateData();

        tvNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPhonePermission()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tvNumber.getText().toString()));
                    startActivity(intent);
                }
            }
        });

    }

    private void populateData() {
        tvTeeth.setText(animalItem.getTeeths());
        tvPrice.setText(animalItem.getPrice() + " PKR");
        tvNumber.setText(animalItem.getOwnerContact());
        tvName.setText(animalItem.getOwnerName());
        tvWeight.setText(animalItem.getWeight() + " KG");
        tvDescription.setText(animalItem.getRemarks());
        if(animalItem.getIsFeatured() == 1)
        {
            imgFeatured.setVisibility(View.VISIBLE);
        }
    }

    private boolean checkPhonePermission() {
        if (ContextCompat.checkSelfPermission(PostDetails.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(PostDetails.this, new String[]{Manifest.permission.CALL_PHONE}, 100);
            return false;
        }
    }
}
