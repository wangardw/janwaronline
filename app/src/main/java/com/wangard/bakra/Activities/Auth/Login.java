package com.wangard.bakra.Activities.Auth;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.wangard.bakra.Activities.DashBoard;
import com.wangard.bakra.AppController;
import com.wangard.bakra.Models.Seller.SellerResponse;
import com.wangard.bakra.R;
import com.wangard.bakra.Utils.Constants;
import com.wangard.bakra.Utils.SnappyDBUtil;
import com.wangard.bakra.Utils.Util;
import com.wangard.bakra.Utils.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity implements View.OnClickListener {

    private Button btnLogin;
    private TextView tvSignUp;
    private EditText etPassword;
    private TextView tvPhone;
    private String phone, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initial();
    }

    private void initial() {

        tvPhone = findViewById(R.id.tvPhone);
        tvPhone.setOnClickListener(this);

        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        tvSignUp = findViewById(R.id.tvSignUp);
        tvSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin: {
                if (validation())
                    loginSeller();
                break;
            }
            case R.id.tvSignUp: {
                startActivity(new Intent(Login.this, SignUp.class));
                break;
            }
            case R.id.tvPhone: {
                tvPhone.setError(null);
                tvPhone.setText("");
                openAccountKit();
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) { // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage = "";
            if (loginResult.getError() != null) {
                if (loginResult.getError().getErrorType().getMessage() != null)
                    tvPhone.setError(loginResult.getError().getErrorType().getMessage());
            } else if (loginResult.wasCancelled()) {
                tvPhone.setError("Number verification cancel");
            } else {
                getPhoneNumber();
            }
        }
    }

    private void getPhoneNumber() {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                String accountKitId = account.getId();
                PhoneNumber phoneNumber = account.getPhoneNumber();
                if (phoneNumber != null) {
                    String phoneNumberString = phoneNumber.toString();
                    tvPhone.setText(phoneNumberString);
                }
            }

            @Override
            public void onError(final AccountKitError error) {
                tvPhone.setError(error.getUserFacingMessage());
            }
        });
    }

    private void openAccountKit() {
        final Intent intent = new Intent(Login.this, AccountKitActivity.class);

        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder = new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE, AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, 101);
    }

    private void loginSeller() {
        if (!Util.isConnectingToInternet(this)) {
            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
            return;
        }
        final ProgressDialog dialog = ProgressDialog.show(Login.this, "Logging into your account", "Please wait...", true);
        dialog.setCancelable(false);
        Call<SellerResponse> call = AppController.getInstance().getApiService().getSellerLoginResponse(phone, password);
        call.enqueue(new Callback<SellerResponse>()
        {
            @Override
            public void onResponse(Call<SellerResponse> call, Response<SellerResponse> response) {
                dialog.dismiss();
                try {
                    Util.showToastMsg(getApplicationContext(), response.body().getMessage());
                    if (response.body().isStatus()) {

                        Utilities.getInstance(getApplicationContext()).saveIntegerPreferences(Constants.PREF_USER_STATE, 3);
                        SnappyDBUtil.saveObject(Constants.USER_DATA, response.body().getSellerData());
                        startActivity(new Intent(Login.this, DashBoard.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();
                        return;
                    }
                } catch (Exception e) {
                    Util.showToastMsg(getApplicationContext(), Constants.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SellerResponse> call, Throwable t) {
                dialog.dismiss();
                Util.showToastMsg(getApplicationContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    private boolean validation() {
        phone = tvPhone.getText().toString();
        password = etPassword.getText().toString();

        if (!Util.isValidMobile(phone)) {
            tvPhone.setError("Valid phone number required");
            tvPhone.requestFocus();
            return false;
        }
        if (password.isEmpty()) {
            etPassword.setError("Password not found");
            etPassword.requestFocus();
            return false;
        }
        return true;
    }
}
