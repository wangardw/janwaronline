package com.wangard.bakra.Activities.Auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.wangard.bakra.Activities.DashBoard;
import com.wangard.bakra.AppController;
import com.wangard.bakra.Models.Seller.SellerResponse;
import com.wangard.bakra.R;
import com.wangard.bakra.Utils.Constants;
import com.wangard.bakra.Utils.SnappyDBUtil;
import com.wangard.bakra.Utils.Util;
import com.wangard.bakra.Utils.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUp extends AppCompatActivity implements View.OnClickListener {

    private Spinner spnLocation;
    private Button btnSignUp;
    private ImageButton btnBack;
    private TextView tvPhone;

    private EditText etName, etEmail, etArea, etPassword, etConfirmPassword;
    private String name, email, phone, area, password, confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initial();
    }

    private void initial() {

        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        etArea = findViewById(R.id.etArea);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);

        tvPhone = findViewById(R.id.tvPhone);
        tvPhone.setOnClickListener(this);

        btnSignUp = findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(this);
        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);


        spnLocation = findViewById(R.id.spnLocation);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(SignUp.this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.locationsArray));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnLocation.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnSignUp: {
                if (validation())
                    signUpSeller();
                break;
            }
            case R.id.btnBack: {
                finish();
                break;
            }
            case R.id.tvPhone: {
                tvPhone.setError(null);
                tvPhone.setText("");
                openAccountKit();
                break;
            }
        }
    }

    private void openAccountKit() {
        final Intent intent = new Intent(SignUp.this, AccountKitActivity.class);

        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder = new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE, AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, 101);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) { // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage = "";
            if (loginResult.getError() != null) {
                if (loginResult.getError().getErrorType().getMessage() != null)
                    tvPhone.setError(loginResult.getError().getErrorType().getMessage());
            } else if (loginResult.wasCancelled()) {
                tvPhone.setError("Number verification cancel");
            } else {
                getPhoneNumber();
            }
        }
    }

    private void getPhoneNumber() {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                String accountKitId = account.getId();
                PhoneNumber phoneNumber = account.getPhoneNumber();
                if (phoneNumber != null) {
                    String phoneNumberString = phoneNumber.toString();
                    tvPhone.setText(phoneNumberString);
                }
            }

            @Override
            public void onError(final AccountKitError error) {
                tvPhone.setError(error.getUserFacingMessage());
            }
        });
    }

    private void signUpSeller() {
        if (!Util.isConnectingToInternet(this)) {
            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
            return;
        }
        final ProgressDialog dialog = ProgressDialog.show(SignUp.this, "Creating new account", "Please wait...", true);
        dialog.setCancelable(false);
        Call<SellerResponse> call = AppController.getInstance().getApiService().getSellerSignUpResponse(name, phone, area, email, "seller", password, "");
        call.enqueue(new Callback<SellerResponse>() {
            @Override
            public void onResponse(Call<SellerResponse> call, Response<SellerResponse> response) {
                dialog.dismiss();
                try {
                    Util.showToastMsg(getApplicationContext(), response.body().getMessage());
                    if (response.body().isStatus()) {
                        Utilities.getInstance(getApplicationContext()).saveIntegerPreferences(Constants.PREF_USER_STATE, 3);
                        SnappyDBUtil.saveObject(Constants.USER_DATA, response.body().getSellerData());
                        startActivity(new Intent(SignUp.this, DashBoard.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();
                        return;
                    }
                } catch (Exception e) {
                    Util.showToastMsg(getApplicationContext(), Constants.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SellerResponse> call, Throwable t) {
                dialog.dismiss();
                Util.showToastMsg(getApplicationContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    private boolean validation() {

        name = etName.getText().toString();
        email = etEmail.getText().toString();
        phone = tvPhone.getText().toString();
        area = spnLocation.getSelectedItem().toString();
        Log.d("",area);
        password = etPassword.getText().toString();
        confirmPassword = etConfirmPassword.getText().toString();


        if (name.isEmpty()) {

            etName.setError("Missing Name");
            etName.requestFocus();
            return false;
        }
        if (!Util.isValidMobile(phone)) {
            tvPhone.setError("Valid phone number required");
            tvPhone.requestFocus();
            return false;
        }
        if (area.isEmpty()) {

            etArea.setError("Missing area name");
            etArea.requestFocus();
            return false;
        }
        if (password.length() < 6) {
            etPassword.setError("Password must have minimum six characters");
            etPassword.requestFocus();
            return false;
        }
        if (!confirmPassword.equalsIgnoreCase(password)) {
            etConfirmPassword.setError("No matching password");
            etConfirmPassword.requestFocus();
            return false;
        }
        return true;
    }
}
