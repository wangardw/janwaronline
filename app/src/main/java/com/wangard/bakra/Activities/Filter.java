package com.wangard.bakra.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.wangard.bakra.R;

public class Filter extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private TextView tvAreas;
    private CheckBox cbGoat, cbCow, cbCamel;
    private RadioGroup rgPriceRange, rgPriceRangeButcher;
    private int selectedFragment = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        init();
    }

    @Override
    public void onBackPressed() {

        setResult(RESULT_CANCELED);
        finish();
    }

    void init() {
        selectedFragment = getIntent().getExtras().getInt("fragment");
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvAreas = findViewById(R.id.tvAreas);
        tvAreas.setOnClickListener(this);

        cbGoat = findViewById(R.id.cbGoat);
        cbCow = findViewById(R.id.cbCow);
        cbCamel = findViewById(R.id.cbCamel);

        rgPriceRange = findViewById(R.id.rgPriceRange);
        rgPriceRangeButcher = findViewById(R.id.rgPriceRangeButcher);

        if (selectedFragment == 0) {
            rgPriceRange.setVisibility(View.VISIBLE);
        } else {
            rgPriceRangeButcher.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvAreas: {
                startActivityForResult(new Intent(Filter.this, FilterAreas.class), 101);
                break;
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter_check, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_check) {

            if (selectedFragment == 0 && tvAreas.getText().toString().isEmpty() && getSelectedCategoryIds().isEmpty() && getMinMaxAnimal(true).isEmpty() && getMinMaxAnimal(false).isEmpty()) {
                onBackPressed();
                return true;
            }
            if (selectedFragment == 1 && tvAreas.getText().toString().isEmpty() && getSelectedCategoryIds().isEmpty() && getMinMaxButcher(true).isEmpty() && getMinMaxButcher(false).isEmpty()) {
                onBackPressed();
                return true;
            }
            Intent intent = getIntent();
            intent.putExtra("fragment", selectedFragment);
            intent.putExtra("areas", tvAreas.getText().toString());
            intent.putExtra("categoryIds", getSelectedCategoryIds());
            if (selectedFragment == 0) {
                intent.putExtra("min", getMinMaxAnimal(true));
                intent.putExtra("max", getMinMaxAnimal(false));
            } else {
                intent.putExtra("min", getMinMaxButcher(true));
                intent.putExtra("max", getMinMaxButcher(false));
            }
            setResult(RESULT_OK, intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String getMinMaxAnimal(boolean isMin) {

        if (rgPriceRange.getCheckedRadioButtonId() == R.id.rbFifty) {
            if (isMin)
                return "0";
            return "50000";
        } else if (rgPriceRange.getCheckedRadioButtonId() == R.id.rbLac) {
            if (isMin)
                return "50000";
            return "100000";
        } else if (rgPriceRange.getCheckedRadioButtonId() == R.id.rbTwoLac) {
            if (isMin)
                return "100000";
            return "200000";
        } else if (rgPriceRange.getCheckedRadioButtonId() == R.id.rbTwoLacPlus) {
            if (isMin)
                return "200000";
            return "1000000";
        }
        return "";
    }

    private String getMinMaxButcher(boolean isMin) {

        if (rgPriceRangeButcher.getCheckedRadioButtonId() == R.id.rbFive) {
            if (isMin)
                return "0";
            return "5000";
        } else if (rgPriceRangeButcher.getCheckedRadioButtonId() == R.id.rbTen) {
            if (isMin)
                return "5000";
            return "10000";
        } else if (rgPriceRangeButcher.getCheckedRadioButtonId() == R.id.rbFifteen) {
            if (isMin)
                return "10000";
            return "15000";
        } else if (rgPriceRangeButcher.getCheckedRadioButtonId() == R.id.rbTwenty) {
            if (isMin)
                return "15000";
            return "20000";
        } else if (rgPriceRangeButcher.getCheckedRadioButtonId() == R.id.rbTwentyPlus) {
            if (isMin)
                return "20000";
            return "100000";
        }
        return "";
    }

    private String getSelectedCategoryIds() {
        String selectedCategoryIds = "";
        if (cbGoat.isChecked()) {
            selectedCategoryIds = selectedCategoryIds+"1";
        }
        if (cbCow.isChecked()) {
            selectedCategoryIds = selectedCategoryIds+"2,";
        }
        if (cbCamel.isChecked()) {
            selectedCategoryIds = selectedCategoryIds +"3,";
        }
        return selectedCategoryIds;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == RESULT_OK) {
            tvAreas.setText(data.getExtras().getString("areas"));
        }
    }
}
