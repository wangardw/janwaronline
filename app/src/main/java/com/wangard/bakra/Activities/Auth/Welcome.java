package com.wangard.bakra.Activities.Auth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wangard.bakra.Activities.DashBoard;
import com.wangard.bakra.R;
import com.wangard.bakra.Utils.Constants;
import com.wangard.bakra.Utils.Utilities;

public class Welcome extends AppCompatActivity implements View.OnClickListener {

    private Button btnSellerSignUp, btnSellerLogin, btnButcherSignUp;
    private TextView tvSkip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        initial();
    }

    private void initial() {
        btnSellerSignUp = findViewById(R.id.btnSellerSignUp);
        btnSellerSignUp.setOnClickListener(this);
        btnSellerLogin = findViewById(R.id.btnSellerLogin);
        btnSellerLogin.setOnClickListener(this);
        btnButcherSignUp = findViewById(R.id.btnButcherSignUp);
        btnButcherSignUp.setOnClickListener(this);
        tvSkip = findViewById(R.id.tvSkip);
        tvSkip.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSellerSignUp: {
                startActivity(new Intent(Welcome.this, SignUp.class));
                finish();
                break;
            }
            case R.id.btnSellerLogin: {
                startActivity(new Intent(Welcome.this, Login.class));
                finish();
                break;
            }
            case R.id.btnButcherSignUp: {
                startActivity(new Intent(Welcome.this, ButcherSignUp.class).putExtra("isWelcome", true));
                finish();
                break;
            }
            case R.id.tvSkip: {
                Utilities.getInstance(getApplicationContext()).saveIntegerPreferences(Constants.PREF_USER_STATE, 2);
                startActivity(new Intent(Welcome.this, DashBoard.class));
                finish();
                break;
            }
        }
    }
}
