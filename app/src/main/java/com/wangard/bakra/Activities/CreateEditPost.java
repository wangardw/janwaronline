package com.wangard.bakra.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.wangard.bakra.Models.Animals.AnimalItem;
import com.wangard.bakra.R;

public class CreateEditPost extends AppCompatActivity {

    private AnimalItem animalItem;
    private boolean isEdit = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit_post);

        initial();
    }

    private void initial()
    {
        animalItem = (AnimalItem) getIntent().getSerializableExtra("animalItem");
        isEdit = (animalItem == null) ? false : true;

        populateData();
    }

    private void populateData()
    {
        if(isEdit)
        {
            Log.d("","");
            return;
        }
        Log.d("","");
    }
}
