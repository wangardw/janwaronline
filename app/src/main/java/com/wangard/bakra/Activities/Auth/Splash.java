package com.wangard.bakra.Activities.Auth;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.wangard.bakra.Activities.DashBoard;
import com.wangard.bakra.R;
import com.wangard.bakra.Utils.Constants;
import com.wangard.bakra.Utils.Utilities;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Splash extends AppCompatActivity {

    // Butcher registered = 1 , Guest User = 2 , Login user or super user = 3
    private int userState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.wangard.bakra", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }


        userState = Utilities.getInstance(Splash.this).getIntegerPreferences(Constants.PREF_USER_STATE);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(1000);
                    if (userState == 0)
                    {
                        startActivity(new Intent(Splash.this, Welcome.class));
                    }
                    else if(userState == 1)
                    {
                        startActivity(new Intent(Splash.this, DashBoard.class));
                    }
                    else if(userState == 2)
                    {
                        startActivity(new Intent(Splash.this, DashBoard.class));
                    }
                    else if(userState == 3)
                    {
                        startActivity(new Intent(Splash.this, DashBoard.class));
                    }
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
}