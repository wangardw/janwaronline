package com.wangard.bakra.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;
import com.wangard.bakra.Activities.Auth.ButcherSignUp;
import com.wangard.bakra.Activities.Auth.Login;
import com.wangard.bakra.Activities.Auth.SignUp;
import com.wangard.bakra.Activities.Auth.Splash;
import com.wangard.bakra.Fragments.FragmentAnimals;
import com.wangard.bakra.Fragments.FragmentButchers;
import com.wangard.bakra.Models.Seller.SellerData;
import com.wangard.bakra.R;
import com.wangard.bakra.Utils.Constants;
import com.wangard.bakra.Utils.SnappyDBUtil;
import com.wangard.bakra.Utils.Utilities;

import de.hdodenhof.circleimageview.CircleImageView;

public class DashBoard extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private Toolbar toolbar;
    private FloatingActionButton btnAddAnimal;
    private TextView tvName, tvPhone;
    private CircleImageView imgUser;
    private ViewPager viewPager;
    private FragmentAnimals fragmentAnimals;
    private FragmentButchers fragmentButchers;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        initial();

    }

    private void initial() {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

//        ViewPager and Tabs
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

//        FloatingActionButton
        btnAddAnimal = findViewById(R.id.btnAddAnimal);
        btnAddAnimal.setOnClickListener(this);

//        Drawer
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        tvName = headerView.findViewById(R.id.tvName);
        tvName.setOnClickListener(this);
        tvPhone = headerView.findViewById(R.id.tvPhone);
        imgUser = headerView.findViewById(R.id.imgUser);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        populateData();
    }

    private void populateData() {
        if (Utilities.getInstance(DashBoard.this).getIntegerPreferences(Constants.PREF_USER_STATE) == 3) {
            SellerData sellerData = SnappyDBUtil.getObject(Constants.USER_DATA, SellerData.class);
            tvPhone.setVisibility(View.VISIBLE);
            tvName.setText(sellerData.getName());
            tvPhone.setText(sellerData.getPhoneNumber());
            if (sellerData.getImage().contains("http"))
                Picasso.get().load(sellerData.getImage()).error(R.drawable.ic_user_placehlder).placeholder(R.drawable.ic_user_placehlder).into(imgUser);
        }
    }

    private void openDialog(String message) {

        AlertDialog.Builder b = new AlertDialog.Builder(DashBoard.this)
                .setTitle("Account Caution")
                .setMessage(message)
                .setPositiveButton("Create New",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                startActivity(new Intent(DashBoard.this, SignUp.class));
                                dialog.dismiss();
                            }
                        }
                )
                .setNegativeButton("Login",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                startActivity(new Intent(DashBoard.this, Login.class));
                                dialog.dismiss();
                            }
                        }
                );
        b.show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.dash_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_profile) {
            if (Utilities.getInstance(DashBoard.this).getIntegerPreferences(Constants.PREF_USER_STATE) == 3) {
                startActivityForResult(new Intent(DashBoard.this, Profile.class), 102);
                return true;
            }
            openDialog("Create or login into your existing account");
            return true;
        } else if (id == R.id.action_filter) {

            startActivityForResult(new Intent(DashBoard.this, Filter.class).putExtra("fragment", viewPager.getCurrentItem()), 101);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == RESULT_OK) {
            if (data.getExtras().getInt("fragment") == 0) {
                fragmentAnimals.setArea(data.getExtras().getString("areas"));
                fragmentAnimals.setCategoryIds(data.getExtras().getString("categoryIds"));
                fragmentAnimals.setMin(data.getExtras().getString("min"));
                fragmentAnimals.setMax(data.getExtras().getString("max"));
                fragmentAnimals.recall();
            } else if (data.getExtras().getInt("fragment") == 1) {
                fragmentButchers.setArea(data.getExtras().getString("areas"));
                fragmentButchers.setCategoryIds(data.getExtras().getString("categoryIds"));
                fragmentButchers.setMin(data.getExtras().getString("min"));
                fragmentButchers.setMax(data.getExtras().getString("max"));
                fragmentButchers.recall();
            }
        } else if (requestCode == 102 && resultCode == RESULT_OK) {
            populateData();
        }
    }

    private boolean checkPermissions() {
        if (ContextCompat.checkSelfPermission(DashBoard.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(DashBoard.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(DashBoard.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
            return false;
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        switch (id) {

            case R.id.nav_myAds: {
                if (Utilities.getInstance(DashBoard.this).getIntegerPreferences(Constants.PREF_USER_STATE) == 3) {
                    if (checkPermissions())
                        startActivity(new Intent(DashBoard.this, MyPosts.class));
                    return true;
                }
                openDialog("Create or login into your existing account to view the ads you posted");
                break;
            }
            case R.id.nav_howWork: {


                break;
            }
            case R.id.nav_addAnimal: {
                if (Utilities.getInstance(DashBoard.this).getIntegerPreferences(Constants.PREF_USER_STATE) == 3) {
                    if (checkPermissions())
                        startActivity(new Intent(DashBoard.this, PostAdd.class).putExtra("isEdit", false));
                    return true;
                }
                openDialog("You need have an account before you post an ad");
                break;
            }
            case R.id.nav_addButcher: {
                if (checkPermissions())
                    startActivity(new Intent(DashBoard.this, ButcherSignUp.class).putExtra("isWelcome", false));
                break;
            }
            case R.id.nav_privacy: {

                break;
            }
            case R.id.nav_contactUs: {

                break;
            }
            case R.id.nav_aboutUs: {

                break;
            }
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddAnimal: {
                addButtonAction();
                break;
            }
            case R.id.tvName: {
                if (Utilities.getInstance(DashBoard.this).getIntegerPreferences(Constants.PREF_USER_STATE) == 3) {
                    startActivityForResult(new Intent(DashBoard.this, Profile.class), 102);
                    break;
                }
                openDialog("Create or login into your existing account");
                break;
            }
        }
    }

    private void addButtonAction() {
        if (viewPager.getCurrentItem() == 1) {
            if (checkPermissions())
                startActivity(new Intent(DashBoard.this, ButcherSignUp.class).putExtra("isWelcome", false));
            return;
        }
        if (Utilities.getInstance(DashBoard.this).getIntegerPreferences(Constants.PREF_USER_STATE) == 3) {
            if (checkPermissions())
                startActivity(new Intent(DashBoard.this, PostAdd.class).putExtra("isEdit", false));
            return;
        }
        openDialog("You need have an account before you post an ad");
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(Context context, FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                fragmentAnimals = new FragmentAnimals();
                return fragmentAnimals;
            } else {
                fragmentButchers = new FragmentButchers();
                return fragmentButchers;
            }

        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {

            if (position == 0)
                return "Animals";
            return "Butchers";
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }
    }
}
