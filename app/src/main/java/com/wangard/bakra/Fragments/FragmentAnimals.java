package com.wangard.bakra.Fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.wangard.bakra.Activities.PostDetails;
import com.wangard.bakra.Adapters.AdapterAnimals;
import com.wangard.bakra.AppController;
import com.wangard.bakra.Models.Animals.AnimalItem;
import com.wangard.bakra.Models.Animals.AnimalsResponse;
import com.wangard.bakra.R;
import com.wangard.bakra.Utils.Constants;
import com.wangard.bakra.Utils.Util;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentAnimals extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AdapterAnimals.AnimalCallBack, View.OnClickListener {

    private View view;

    private SwipeRefreshLayout refresher;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ArrayList<AnimalItem> list;
    private AdapterAnimals adapter;
    private ViewGroup llNothing;
    private Button btnRefresh;
    private ImageButton ibReTry;
    private int currentPage = 1, lastPage = 0;
    private Boolean isLoading = false, isRefresh = false;
    private String categoryIds = "", area = "", min = "", max = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_animals, container, false);
        initial();
        return view;
    }

    private void initial() {
        refresher = view.findViewById(R.id.refresher);
        refresher.setOnRefreshListener(this);

//        nothing to show and refresh layout
        llNothing = view.findViewById(R.id.llNothing);
        btnRefresh = view.findViewById(R.id.btnRefresh);
        btnRefresh.setOnClickListener(this);
        ibReTry = view.findViewById(R.id.ibReTry);
        ibReTry.setOnClickListener(this);

//        recyclerView and pagination
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1)) {
                    loadMore();
                }
            }
        });
        list = new ArrayList<>();
        adapter = new AdapterAnimals(list, this, false);
        recyclerView.setAdapter(adapter);
        progressBar = view.findViewById(R.id.progressBar);

        refresher.setRefreshing(true);
        getALlAnimals();
    }


    public void setCategoryIds(String categoryIds) {
        this.categoryIds = categoryIds;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public void recall() {
        refresher.setRefreshing(true);
        currentPage = 1;
        isRefresh = true;
        getALlAnimals();
    }

    private void loadMore() {
        if (currentPage < lastPage & isLoading == false) {
            progressBar.setVisibility(View.VISIBLE);
            currentPage = currentPage + 1;
            getALlAnimals();
        }
    }

    private void getALlAnimals() {
        if (!Util.isConnectingToInternet(getContext())) {
            isLoading = false;
            refresher.setRefreshing(false);
            progressBar.setVisibility(View.GONE);
            ibReTry.setVisibility(View.VISIBLE);
            Util.showToastMsg(getContext(), Constants.kStringNetworkConnectivityError);
            return;
        }
        isLoading = true;
        Call<AnimalsResponse> call = AppController.getInstance().getApiService().getAnimalsListResponse(currentPage, categoryIds, area, min, max);
        call.enqueue(new Callback<AnimalsResponse>() {
            @Override
            public void onResponse(Call<AnimalsResponse> call, Response<AnimalsResponse> response) {
                isLoading = false;
                refresher.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                ibReTry.setVisibility(View.GONE);
                if (isRefresh) {
                    list.clear();
                    isRefresh = false;
                }
                try {
                    currentPage = response.body().getAnimalsData().getCurrentPage();
                    lastPage = response.body().getAnimalsData().getLastPage();
                    if (response.body().isStatus()) {
                        list.addAll(response.body().getAnimalsData().getAnimalList());
                        adapter.notifyDataSetChanged();
                    } else if (list.size() > 1) {
                        ibReTry.setVisibility(View.VISIBLE);
                    } else if (list.size() < 1) {
                        llNothing.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    Util.showToastMsg(getContext(), Constants.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AnimalsResponse> call, Throwable t) {
                isLoading = false;
                refresher.setRefreshing(false);
                ibReTry.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                if (list.size() > 1) {
                    ibReTry.setVisibility(View.VISIBLE);
                } else if (list.size() < 1) {
                    llNothing.setVisibility(View.VISIBLE);
                }
                Util.showToastMsg(getContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    @Override
    public void onRefresh() {

        removeFilterAndRefresh();
    }

    @Override
    public void onImageClicked(int position) {

        Intent intent = new Intent(getContext(), PostDetails.class);
        intent.putExtra("postItem", list.get(position));
//        intent.putExtra("imagesList", list.get(position).getImages());
        startActivity(intent);
    }

    @Override
    public void onPhoneClicked(int position) {
        if (checkPhonePermission()) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + list.get(position).getOwnerContact()));
            startActivity(intent);
        }
    }

    @Override
    public void onOptionDelete(int position) {

    }

    @Override
    public void onOptionEdit(int position) {

    }

    private boolean checkPhonePermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 100);
            return false;
        }
    }


    private void removeFilterAndRefresh()
    {
        categoryIds = "";
        area = "";
        min = "";
        max = "";
        recall();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRefresh: {
                llNothing.setVisibility(View.GONE);
                removeFilterAndRefresh();
                break;
            }
            case R.id.ibReTry: {
                ibReTry.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                getALlAnimals();
                break;
            }
        }
    }
}
