package com.wangard.bakra.Fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.wangard.bakra.Adapters.AdapterAnimals;
import com.wangard.bakra.Adapters.AdapterButchers;
import com.wangard.bakra.AppController;
import com.wangard.bakra.Models.Butcher.ButcherItem;
import com.wangard.bakra.Models.Butcher.ButchersResponse;
import com.wangard.bakra.R;
import com.wangard.bakra.Utils.Constants;
import com.wangard.bakra.Utils.Util;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentButchers extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AdapterButchers.ButcherCallBack, View.OnClickListener {


    private View view;

    private SwipeRefreshLayout refresher;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ArrayList<ButcherItem> list;
    private AdapterButchers adapter;
    private ViewGroup llNothing;
    private Button btnRefresh;
    private ImageButton ibReTry;
    private String categoryIds = "", area = "", min = "", max = "";

    private int currentPage = 1, lastPage = 0;
    private Boolean isLoading = false, isRefresh = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_butchers, container, false);
        initial();
        return view;
    }

    public void setCategoryIds(String categoryIds) {
        this.categoryIds = categoryIds;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public void recall() {
        refresher.setRefreshing(true);
        currentPage = 1;
        isRefresh = true;
        getALlButchers();
    }

    private void initial() {

        refresher = view.findViewById(R.id.refresher);
        refresher.setOnRefreshListener(this);


//        nothing to show and refresh layout
        llNothing = view.findViewById(R.id.llNothing);
        btnRefresh = view.findViewById(R.id.btnRefresh);
        btnRefresh.setOnClickListener(this);
        ibReTry = view.findViewById(R.id.ibReTry);
        ibReTry.setOnClickListener(this);


//        recyclerView and pagination
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1)) {
                    loadMore();
                }
            }
        });

        list = new ArrayList<>();
        adapter = new AdapterButchers(list, this);
        recyclerView.setAdapter(adapter);
        progressBar = view.findViewById(R.id.progressBar);

        refresher.setRefreshing(true);
        getALlButchers();
    }


    private void loadMore() {
        if (currentPage < lastPage & isLoading == false) {
            progressBar.setVisibility(View.VISIBLE);
            currentPage = currentPage + 1;
            getALlButchers();
        }
    }

    private void getALlButchers() {
        if (!Util.isConnectingToInternet(getContext())) {
            isLoading = false;
            refresher.setRefreshing(false);
            progressBar.setVisibility(View.GONE);
            ibReTry.setVisibility(View.VISIBLE);
            Util.showToastMsg(getContext(), Constants.kStringNetworkConnectivityError);
            return;
        }
        isLoading = true;
        Call<ButchersResponse> call = AppController.getInstance().getApiService().getButchersListResponse(currentPage, categoryIds, area, min, max);
        call.enqueue(new Callback<ButchersResponse>() {
            @Override
            public void onResponse(Call<ButchersResponse> call, Response<ButchersResponse> response) {
                isLoading = false;
                refresher.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                ibReTry.setVisibility(View.GONE);
                if (isRefresh) {
                    list.clear();
                    isRefresh = false;
                }
                try {
                    currentPage = response.body().getButcherData().getCurrentPage();
                    lastPage = response.body().getButcherData().getLastPage();
                    if (response.body().isStatus()) {
                        list.addAll(response.body().getButcherData().getButcherList());
                        adapter.notifyDataSetChanged();
                    } else if (list.size() > 1) {
                        ibReTry.setVisibility(View.VISIBLE);
                    } else if (list.size() < 1) {
                        llNothing.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    Util.showToastMsg(getContext(), Constants.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ButchersResponse> call, Throwable t) {
                isLoading = false;
                refresher.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                ibReTry.setVisibility(View.GONE);
                if (list.size() > 1) {
                    ibReTry.setVisibility(View.VISIBLE);
                } else if (list.size() < 1) {
                    llNothing.setVisibility(View.VISIBLE);
                }
                Util.showToastMsg(getContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }



    private void removeFilterAndRefresh()
    {
        categoryIds = "";
        area = "";
        min = "";
        max = "";
        recall();
    }
    @Override
    public void onRefresh() {
        removeFilterAndRefresh();
    }

    @Override
    public void onButcherCallClicked(int position) {
        if (checkPhonePermission()) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + list.get(position).getPhoneNumber()));
            startActivity(intent);
        }
    }

    private boolean checkPhonePermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 100);
            return false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRefresh: {
                llNothing.setVisibility(View.GONE);
                removeFilterAndRefresh();
                break;
            }
            case R.id.ibReTry: {
                ibReTry.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                getALlButchers();
                break;
            }
        }
    }
}
