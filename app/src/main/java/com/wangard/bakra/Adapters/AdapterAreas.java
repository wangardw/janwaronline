package com.wangard.bakra.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wangard.bakra.Models.Areas.AreaItem;
import com.wangard.bakra.R;

import java.util.ArrayList;

public class AdapterAreas extends RecyclerView.Adapter<AdapterAreas.MyViewHolder> {

    private ArrayList<AreaItem> list;
    private AreasCallback callback;

    public AdapterAreas(ArrayList<AreaItem> list, AreasCallback callback) {
        this.list = list;
        this.callback = callback;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.area_item, parent, false);
        return new AdapterAreas.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        if (!list.get(position).getName().isEmpty()){
            holder.cbArea.setText(list.get(position).getName());
        }

        holder.cbArea.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                callback.onItemSelectionChange(position, isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        CheckBox cbArea;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            cbArea = itemView.findViewById(R.id.cbArea);

        }
    }

    public interface AreasCallback
    {
        void onItemSelectionChange(int position, boolean isChecked);
    }
}
