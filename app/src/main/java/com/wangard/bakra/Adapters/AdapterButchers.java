package com.wangard.bakra.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.wangard.bakra.Models.Butcher.ButcherCategoryItem;
import com.wangard.bakra.Models.Butcher.ButcherItem;
import com.wangard.bakra.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterButchers extends RecyclerView.Adapter<AdapterButchers.MyViewHolder> {

    private ArrayList<ButcherItem> list;
    private ButcherCallBack callBack;

    public AdapterButchers(ArrayList<ButcherItem> list, ButcherCallBack callBack) {

        this.callBack = callBack;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_butcher, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (!list.get(position).getButcherImage().isEmpty())
            Picasso.get().load(list.get(position).getButcherImage()).error(R.drawable.ic_user_placehlder).placeholder(R.drawable.ic_user_placehlder).into(holder.imgUser);
        holder.tvName.setText(list.get(position).getName());
        holder.tvPhone.setText(list.get(position).getPhoneNumber());
        holder.tvArea.setText(list.get(position).getArea());

        ArrayList<ButcherCategoryItem> priceList = list.get(position).getButcherCategoryList();
        if(priceList.size() > 0)
        {
            holder.recyclerView.setAdapter(new AdapterButchersPrice(priceList));
        }

        holder.tvPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onButcherCallClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView imgUser;
        TextView tvName, tvPhone, tvArea;
        RecyclerView recyclerView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgUser = itemView.findViewById(R.id.imgUser);
            tvName = itemView.findViewById(R.id.tvName);
            tvPhone = itemView.findViewById(R.id.tvPhone);
            tvArea = itemView.findViewById(R.id.tvArea);
            recyclerView = itemView.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext(), RecyclerView.HORIZONTAL, false));

        }
    }

    public interface ButcherCallBack
    {
        void onButcherCallClicked(int position);
    }
}
