package com.wangard.bakra.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wangard.bakra.R;

public class AdapterCreateOrEditImages extends RecyclerView.Adapter<AdapterCreateOrEditImages.MyViewHolder> {


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_create_or_edit_image, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgAnimal;
        View bgProgressBar;
        ProgressBar progressBar;
        ImageButton ibRetry;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imgAnimal = itemView.findViewById(R.id.imgAnimal);
            bgProgressBar = itemView.findViewById(R.id.bgProgressBar);
            progressBar = itemView.findViewById(R.id.progressBar);
            ibRetry = itemView.findViewById(R.id.ibRetry);
        }
    }
}
