package com.wangard.bakra.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.wangard.bakra.Models.Butcher.ButcherCategoryItem;
import com.wangard.bakra.Models.Butcher.ButcherItem;
import com.wangard.bakra.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterButchersPrice extends RecyclerView.Adapter<AdapterButchersPrice.MyViewHolder> {

    private ArrayList<ButcherCategoryItem> list;

    public AdapterButchersPrice(ArrayList<ButcherCategoryItem> list) {

        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_price, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (list.get(position).getCategoryId() == 1) {

            holder.tvAnimalName.setText("Goat/Sheep");
            holder.tvAnimalPrice.setText(list.get(position).getPrice() + " PKR");

        } else if (list.get(position).getCategoryId() == 2) {

            holder.tvAnimalName.setText("Cow/Bull");
            holder.tvAnimalPrice.setText(list.get(position).getPrice() + " PKR");

        } else if (list.get(position).getCategoryId() == 3) {

            holder.tvAnimalName.setText("Camel");
            holder.tvAnimalPrice.setText(list.get(position).getPrice()+ " PKR");
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvAnimalName, tvAnimalPrice;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvAnimalName = itemView.findViewById(R.id.tvAnimalName);
            tvAnimalPrice = itemView.findViewById(R.id.tvAnimalPrice);

        }
    }
}
