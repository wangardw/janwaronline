package com.wangard.bakra.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.wangard.bakra.Models.Animals.AnimalItem;
import com.wangard.bakra.R;

import java.util.ArrayList;

public class AdapterAnimals extends RecyclerView.Adapter<AdapterAnimals.MyViewHolder> {

    private ArrayList<AnimalItem> list;
    private AnimalCallBack callBack;
    private boolean isMyPosts = false;
    private Context context;

    public AdapterAnimals(ArrayList<AnimalItem> list, AnimalCallBack callBack, boolean isMyPosts) {
        this.callBack = callBack;
        this.list = list;
        this.isMyPosts = isMyPosts;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_animals, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        String imgUrl = null;
        if (list.get(position).getImages().size() > 0)
            imgUrl = list.get(position).getImages().get(0).getImageUrl();
        if (imgUrl != null)
            Picasso.get().load(imgUrl).error(R.drawable.ic_img_placeholder).placeholder(R.drawable.ic_img_placeholder).fit().centerCrop().into(holder.imgAnimal);
        holder.tvWeight.setText(list.get(position).getWeight() + " KG");
        holder.tvPrice.setText(list.get(position).getPrice() + " PKR");
        holder.tvName.setText(list.get(position).getOwnerName());
        holder.tvPhone.setText(list.get(position).getOwnerContact());
        holder.tvArea.setText(list.get(position).getOwnerArea());

        if (list.get(position).getIsFeatured() == 0) {
            holder.imgFeatured.setVisibility(View.GONE);
        } else {
            holder.imgFeatured.setVisibility(View.VISIBLE);
        }
        holder.imgAnimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onImageClicked(position);
            }
        });
        holder.tvPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onPhoneClicked(position);
            }
        });
        if (isMyPosts) {
            holder.ibOptions.setVisibility(View.VISIBLE);
        } else {
            holder.ibOptions.setVisibility(View.GONE);
        }
        holder.ibOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu(holder, position);
            }
        });
    }

    private void showMenu(MyViewHolder holder, final int position) {
        //creating a popup menu
        PopupMenu popup = new PopupMenu(context, holder.ibOptions);
        //inflating menu from xml resource
        popup.inflate(R.menu.menu_mypost_options);
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_Edit: {
                        callBack.onOptionEdit(position);
                        return true;
                    }
                    case R.id.action_Delete: {

                        callBack.onOptionDelete(position);
                        return true;
                    }
                    default:
                        return false;
                }
            }
        });
        //displaying the popup
        popup.show();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageButton ibOptions;
        ImageView imgAnimal, imgFeatured;
        TextView tvWeight, tvPrice, tvName, tvPhone, tvArea;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ibOptions = itemView.findViewById(R.id.ibOptions);
            imgAnimal = itemView.findViewById(R.id.imgAnimal);
            tvWeight = itemView.findViewById(R.id.tvWeight);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvName = itemView.findViewById(R.id.tvName);
            tvPhone = itemView.findViewById(R.id.tvPhone);
            tvArea = itemView.findViewById(R.id.tvArea);
            imgFeatured = itemView.findViewById(R.id.imgFeatured);
        }
    }

    public interface AnimalCallBack {

        void onImageClicked(int position);

        void onPhoneClicked(int position);

        void onOptionDelete(int position);

        void onOptionEdit(int position);


    }
}
