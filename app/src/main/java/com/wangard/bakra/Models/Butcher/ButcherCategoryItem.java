package com.wangard.bakra.Models.Butcher;

import com.google.gson.annotations.SerializedName;
public class ButcherCategoryItem {

	@SerializedName("category_id")
	private int categoryId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("butcher_id")
	private int butcherId;

	@SerializedName("price")
	private String price;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	public void setCategoryId(int categoryId){
		this.categoryId = categoryId;
	}

	public int getCategoryId(){
		return categoryId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setButcherId(int butcherId){
		this.butcherId = butcherId;
	}

	public int getButcherId(){
		return butcherId;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"ButcherCategoryItem{" +
			"category_id = '" + categoryId + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",butcher_id = '" + butcherId + '\'' + 
			",price = '" + price + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}