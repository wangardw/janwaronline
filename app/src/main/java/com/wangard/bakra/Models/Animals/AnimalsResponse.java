package com.wangard.bakra.Models.Animals;

import com.google.gson.annotations.SerializedName;

public class AnimalsResponse{

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private boolean status;

	@SerializedName("data")
	private AnimalsData animalsData;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	public void setAnimalsData(AnimalsData animalsData){
		this.animalsData = animalsData;
	}

	public AnimalsData getAnimalsData(){
		return animalsData;
	}

	@Override
 	public String toString(){
		return 
			"AnimalsResponse{" + 
			"message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			",animalsData = '" + animalsData + '\'' + 
			"}";
		}
}