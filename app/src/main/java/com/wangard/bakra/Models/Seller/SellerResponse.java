package com.wangard.bakra.Models.Seller;

import com.google.gson.annotations.SerializedName;

public class SellerResponse{

	@SerializedName("data")
	private SellerData sellerData;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private boolean status;

	public void setSellerData(SellerData sellerData){
		this.sellerData = sellerData;
	}

	public SellerData getSellerData(){
		return sellerData;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SellerResponse{" + 
			"sellerData = '" + sellerData + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}