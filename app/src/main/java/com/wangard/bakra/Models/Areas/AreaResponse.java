package com.wangard.bakra.Models.Areas;

import com.google.gson.annotations.SerializedName;

public class AreaResponse {

    @SerializedName("status")
    private boolean status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private AreaData areaData;

    // Getter Methods
    public boolean getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public AreaData getData() {
        return areaData;
    }

    // Setter Methods
    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(AreaData areaData) {
        this.areaData = areaData;
    }
}

