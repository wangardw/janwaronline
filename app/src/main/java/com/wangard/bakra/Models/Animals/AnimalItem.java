package com.wangard.bakra.Models.Animals;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AnimalItem implements Serializable {

	@SerializedName("images")
	private ArrayList<ImagesItem> images;

	@SerializedName("owner_name")
	private String ownerName;

	@SerializedName("weight")
	private String weight;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("category_id")
	private int categoryId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("price")
	private int price;

	@SerializedName("owner_contact")
	private String ownerContact;

	@SerializedName("owner_area")
	private String ownerArea;

	@SerializedName("teeths")
	private String teeths;

	@SerializedName("id")
	private int id;

	@SerializedName("category")
	private Category category;

	@SerializedName("remarks")
	private String remarks;

	@SerializedName("is_featured")
	private int isFeatured;

	public ArrayList<ImagesItem> getImages() {
		return images;
	}

	public void setImages(ArrayList<ImagesItem> images) {
		this.images = images;
	}

	public void setOwnerName(String ownerName){
		this.ownerName = ownerName;
	}

	public String getOwnerName(){
		return ownerName;
	}

	public void setWeight(String weight){
		this.weight = weight;
	}

	public String getWeight(){
		return weight;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setCategoryId(int categoryId){
		this.categoryId = categoryId;
	}

	public int getCategoryId(){
		return categoryId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setOwnerContact(String ownerContact){
		this.ownerContact = ownerContact;
	}

	public String getOwnerContact(){
		return ownerContact;
	}

	public void setOwnerArea(String ownerArea){
		this.ownerArea = ownerArea;
	}

	public String getOwnerArea(){
		return ownerArea;
	}

	public void setTeeths(String teeths){
		this.teeths = teeths;
	}

	public String getTeeths(){
		return teeths;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setCategory(Category category){
		this.category = category;
	}

	public Category getCategory(){
		return category;
	}

	public void setRemarks(String remarks){
		this.remarks = remarks;
	}

	public String getRemarks(){
		return remarks;
	}

	public void setIsFeatured(int isFeatured){
		this.isFeatured = isFeatured;
	}

	public int getIsFeatured(){
		return isFeatured;
	}

	@Override
 	public String toString(){
		return 
			"AnimalItem{" + 
			"images = '" + images + '\'' + 
			",owner_name = '" + ownerName + '\'' + 
			",weight = '" + weight + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",category_id = '" + categoryId + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",user_id = '" + userId + '\'' + 
			",price = '" + price + '\'' + 
			",owner_contact = '" + ownerContact + '\'' + 
			",owner_area = '" + ownerArea + '\'' + 
			",teeths = '" + teeths + '\'' + 
			",id = '" + id + '\'' + 
			",category = '" + category + '\'' + 
			",remarks = '" + remarks + '\'' + 
			",is_featured = '" + isFeatured + '\'' + 
			"}";
		}
}