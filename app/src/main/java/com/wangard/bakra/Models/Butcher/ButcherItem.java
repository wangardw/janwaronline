package com.wangard.bakra.Models.Butcher;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ButcherItem{

	@SerializedName("area")
	private String area;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("name")
	private String name;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("phone_number")
	private String phoneNumber;

	@SerializedName("id")
	private int id;

	@SerializedName("bakra_price")
	private String bakraPrice;

	@SerializedName("camel_price")
	private String camelPrice;

	@SerializedName("butcher_image")
	private String butcherImage;

	@SerializedName("cow_price")
	private String cowPrice;

	@SerializedName("butcher_category")
	private ArrayList<ButcherCategoryItem> butcherCategoryList;

	public ArrayList<ButcherCategoryItem> getButcherCategoryList() {
		return butcherCategoryList;
	}

	public void setButcherCategoryList(ArrayList<ButcherCategoryItem> butcherCategoryList) {
		this.butcherCategoryList = butcherCategoryList;
	}

	public void setArea(String area){
		this.area = area;
	}

	public String getArea(){
		return area;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber(){
		return phoneNumber;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setBakraPrice(String bakraPrice){
		this.bakraPrice = bakraPrice;
	}

	public String getBakraPrice(){
		return bakraPrice;
	}

	public void setCamelPrice(String camelPrice){
		this.camelPrice = camelPrice;
	}

	public String getCamelPrice(){
		return camelPrice;
	}

	public void setButcherImage(String butcherImage){
		this.butcherImage = butcherImage;
	}

	public String getButcherImage(){
		return butcherImage;
	}

	public void setCowPrice(String cowPrice){
		this.cowPrice = cowPrice;
	}

	public String getCowPrice(){
		return cowPrice;
	}

	@Override
 	public String toString(){
		return 
			"ButcherItem{" + 
			"area = '" + area + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",name = '" + name + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",phone_number = '" + phoneNumber + '\'' + 
			",id = '" + id + '\'' + 
			",bakra_price = '" + bakraPrice + '\'' + 
			",camel_price = '" + camelPrice + '\'' + 
			",butcher_image = '" + butcherImage + '\'' + 
			",cow_price = '" + cowPrice + '\'' + 
			"}";
		}
}