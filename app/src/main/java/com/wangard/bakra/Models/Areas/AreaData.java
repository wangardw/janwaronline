package com.wangard.bakra.Models.Areas;

import com.google.gson.annotations.SerializedName;
import com.wangard.bakra.Models.Butcher.ButcherItem;

import java.util.List;

public class AreaData {

    @SerializedName("current_page")
    private int current_page;
    @SerializedName("data")
    private List<AreaItem> areaList;

    @SerializedName("first_page_url")
    private String first_page_url;
    @SerializedName("from")
    private float from;
    @SerializedName("last_page")
    private int last_page;
    @SerializedName("last_page_url")
    private String last_page_url;
    @SerializedName("next_page_url")
    private String next_page_url = null;
    @SerializedName("path")
    private String path;
    @SerializedName("per_page")
    private float per_page;
    @SerializedName("prev_page_url")
    private String prev_page_url = null;
    @SerializedName("to")
    private float to;
    @SerializedName("total")
    private float total;

    // Getter Methods
    public int getCurrent_page() {
        return current_page;
    }

    public List<AreaItem> getAreaList() {
        return areaList;
    }

    public String getFirst_page_url() {
        return first_page_url;
    }

    public float getFrom() {
        return from;
    }

    public int getLast_page() {
        return last_page;
    }

    public String getLast_page_url() {
        return last_page_url;
    }

    public String getNext_page_url() {
        return next_page_url;
    }

    public String getPath() {
        return path;
    }

    public float getPer_page() {
        return per_page;
    }

    public String getPrev_page_url() {
        return prev_page_url;
    }

    public float getTo() {
        return to;
    }

    public float getTotal() {
        return total;
    }

    // Setter Method
    public void setCurrent_page(int current_page) {
        this.current_page = current_page;
    }

    public void setAreaList(List<AreaItem> areaList) {
        this.areaList = areaList;
    }

    public void setFirst_page_url(String first_page_url) {
        this.first_page_url = first_page_url;
    }

    public void setFrom(float from) {
        this.from = from;
    }

    public void setLast_page(int last_page) {
        this.last_page = last_page;
    }

    public void setLast_page_url(String last_page_url) {
        this.last_page_url = last_page_url;
    }

    public void setNext_page_url(String next_page_url) {
        this.next_page_url = next_page_url;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setPer_page(float per_page) {
        this.per_page = per_page;
    }

    public void setPrev_page_url(String prev_page_url) {
        this.prev_page_url = prev_page_url;
    }

    public void setTo(float to) {
        this.to = to;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
