package com.wangard.bakra.Models.Butcher;

import com.google.gson.annotations.SerializedName;

public class ButchersResponse{

	@SerializedName("data")
	private ButcherData butcherData;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private boolean status;

	public void setButcherData(ButcherData butcherData){
		this.butcherData = butcherData;
	}

	public ButcherData getButcherData(){
		return butcherData;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ButchersResponse{" + 
			"butcherData = '" + butcherData + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}