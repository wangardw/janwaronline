package com.wangard.bakra.NetWorkClient;

import com.google.gson.JsonObject;
import com.wangard.bakra.Models.Animals.AnimalsResponse;
import com.wangard.bakra.Models.Areas.AreaResponse;
import com.wangard.bakra.Models.Butcher.ButchersResponse;
import com.wangard.bakra.Models.Seller.SellerResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface ApiInterface {

    //    Seller signUp
    @FormUrlEncoded
    @POST("create-user")
    Call<SellerResponse> getSellerSignUpResponse(@Field("name") String name,
                                                 @Field("phone_number") String phone,
                                                 @Field("area") String area,
                                                 @Field("email") String email,
                                                 @Field("type") String type,
                                                 @Field("password") String password,
                                                 @Field("image") String image);

    //    Seller update
    @FormUrlEncoded
    @POST("user/update")
    Call<SellerResponse> getSellerUpdateResponse(@Field("user_id") int userId,
                                                 @Field("name") String name,
                                                 @Field("email") String email,
                                                 @Field("area") String area,
                                                 @Field("phone_number") String phoneNumber,
                                                 @Field("image") String image);

    //    Seller update
    @GET("user/ads/count/")
    Call<JsonObject> getMyPostsCountResponse(@Query("user_id") int userId);

    //    Seller butcher
    @FormUrlEncoded
    @POST("create-butcher")
    Call<JsonObject> getButcherSignUpResponse(@Field("name") String name,
                                              @Field("phone_number") String phone,
                                              @Field("area") String area,
                                              @Field("butcher_image") String butcherImage,
                                              @Field("butcher_price") String butcherPrice);

    //    Seller login
    @FormUrlEncoded
    @POST("auth/login")
    Call<SellerResponse> getSellerLoginResponse(@Field("phone_number") String phone,
                                                @Field("password") String password);

    //    Delete Post
    @FormUrlEncoded
    @POST("delete-bakra")
    Call<JsonObject> getDeletePostResponse(@Field("id") int id);


    @FormUrlEncoded
    @POST("create-bakra")
    Call<JsonObject> getAddAnimalResponse(@Field("user_id") int user_id,
                                          @Field("owner_area") String area,
                                          @Field("teeths") String teeths,
                                          @Field("price") String price,
                                          @Field("weight") String weight,
                                          @Field("category_id") String category_id,
                                          @Field("remarks") String remarks,
                                          @Field("is_featured") String is_featured,
                                          @Field("owner_contact") String owner_contact,
                                          @Field("owner_name") String owner_name,
                                          @Field("images[]") String[] images);

    @FormUrlEncoded
    @PUT("update-bakra")
    Call<JsonObject> getUpdateAnimalResponse(@Field("id") int postId,
                                             @Field("user_id") int userId,
                                             @Field("owner_area") String area,
                                             @Field("teeths") String teeths,
                                             @Field("price") String price,
                                             @Field("weight") String weight,
                                             @Field("category_id") String category_id,
                                             @Field("remarks") String remarks,
                                             @Field("is_featured") String is_featured,
                                             @Field("owner_contact") String owner_contact,
                                             @Field("owner_name") String owner_name,
                                             @Field("images[]") String[] images);


    //    get butchers list
    @GET("get-butcher")
    Call<ButchersResponse> getButchersListResponse(@Query("page") int page,
                                                   @Query("category_id") String categoryId,
                                                   @Query("area") String area,
                                                   @Query("min") String min,
                                                   @Query("max") String max);
    //    get butchers list

    @GET("get-all-bakra")
    Call<AnimalsResponse> getAnimalsListResponse(@Query("page") int page,
                                                 @Query("category_id") String categoryId,
                                                 @Query("area") String area,
                                                 @Query("min") String min,
                                                 @Query("max") String max);

    //get my posts
    @GET("user/ads")
    Call<AnimalsResponse> getMyPostsResponse(@Query("page") int page,
                                             @Query("user_id") int userId);

    //    get areas list
    @GET("area")
    Call<AreaResponse> getAreasResponse(@Query("page") int page);


}
