package com.wangard.bakra.Utils;

public interface Constants {

    String BASE_URL = "http://bakramandi.zakmall.com/api/";
    String BASE_URL_WEB = "https://admin.system.hr/";

    String kStringNetworkConnectivityError = "Please make sure your device is connected with internet.";
    String EXCEPTION = "Exception";
    String EXCEPTION_MESSAGE = "Something went wrong";
    String SERVER_EXCEPTION_MESSAGE = "Something went wrong, server not responding";
    // Snappy save User data
    String USER_DATA = "userData";
    // Preferences
    String PREF_USER_STATE = "userState"; // Butcher registered = 1 , Guest User = 2 , Login user or super user = 3
    String PREF_IS_USER_LOGIN = "isUserLogIn";
}
